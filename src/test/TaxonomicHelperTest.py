'''
Created on Jul 12, 2019

@author: bo
'''
import unittest
from helper import TaxonomicHelper, Binning, TaxonomicProfiling, BinningHelper


class TaxonomicHelperTest(unittest.TestCase):

    def testTaxonomicHelper(self):
        print("test testTaxonomicHelper")
        helper = TaxonomicHelper()
        taxid = '1126616.8'
        assert helper.get_rank(taxid) =='strain'
        assert  helper.get_length(taxid)>0
        print(taxid, helper.get_taxpath(taxid))

        taxid = 1126616
        assert helper.get_rank(taxid) =='species'
        assert  helper.get_length(taxid)>0
        print(taxid, helper.get_taxpath(taxid))


    def testBinningHelper(self):
        print("test testBinningHelper")
        helper = BinningHelper()
        taxid = 1126616
        assert helper.is_in_rank(taxid, 'strain')
        assert helper.get_tax_bin_id(taxid, 'strain') =='1126616.8'
        
        assert helper.is_in_rank(taxid, 'species')
        assert helper.get_tax_bin_id(taxid, 'species') =='1126616'


    def testBinning(self):
        binning = Binning()
        binning.set_comment("a comment")
        binning.set_sample_id("sample10")
        binning.add('r01', taxid=1)
        binning.add('r02', taxid="3.1")
        binning.add('r011', taxid=45)
        
        binning.write("/tmp/testbining.txt")
        
        lst = Binning.read("/tmp/testbining.txt")
        assert len(lst) == 3
        for u in lst:
            assert len(u) == 2
        
    def testTaxonomicProfiling(self):
        taxprof = TaxonomicProfiling()
        taxprof.set_comment("a comment")
        taxprof.set_sample_id("sample10")
        taxprof.add('r01', 'superkingdom', "2.2", 'Bacteria', 12.222)
        taxprof.add('r02', 'class', '2|1224|28211|356', 'Bacteria', 112.222)
        taxprof.add('r03', 'order', 2, 'Bacteria|Proteobacteria|Alphaproteobacteria', 122.222)
        
        taxprof.write("/tmp/testtaxprof.txt")
        
        lst = taxprof.read("/tmp/testtaxprof.txt")
        assert len(lst) == 3
        for u in lst:
            assert len(u) == 5        


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
