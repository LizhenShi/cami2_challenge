import config
import utils
from task import Task
import os

from utils import check_output_file_exists, get_input_file
import gc
import sys
from step12f_assembly_seq_predict_full import SEQ_GSA_LENGTHS
from step20_make_rank_pred_assembly import read_pred
from helper import BinningHelper
import shutil
import pandas as pd 
import numpy as np 


def find_best_rank(u):
    global target_rank, binhelper
    maxp = -1
    maxr = -1
    for k, v in u.items():
        if binhelper.is_in_rank(k, target_rank):
            if maxp < v:
                maxp = v
                maxr = k
    return maxr


def make_rank_predict_with_vrius(prefix, logger):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.allpred.rank'.format(prefix)), logger):
            return

        fname = get_input_file(os.path.join(config.PRED_PATH, '{}.pred'.format(prefix)), logger)
        logger.info("reading prediction from " + fname)
        pred = read_pred(fname, only_bacteria=False)
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))

        global target_rank, binhelper
        binhelper = BinningHelper()
        RANKS = binhelper.ranks
        for target_rank in RANKS:
            logger.info("to find best rank pred for " + target_rank)
            pred[target_rank] = utils.parallell_for(pred['prob'].values, find_best_rank, n_jobs=utils.get_num_thread())
            pred[target_rank].value_counts().head(10)
        
        pred = pred[RANKS]
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))
        
        df = pred
        tmpoutpath = os.path.join(config.LOCAL_PATH, "{}.allpred.rank.gz".format(prefix))
        logger.info("writing " + tmpoutpath)
        df.to_csv(tmpoutpath, compression='gzip')
        outpath = os.path.join(config.PRED_PATH, "{}.allpred.rank.gz".format(prefix))
        shutil.move(tmpoutpath, outpath)

        
def make_cami2_patmg_short_rank_pred_with_virus():               
        
    def f(logger):
        prefix = 'patmg_CAMI2_short_read_merge'
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.allpred.rank'.format(prefix)), logger):
            return
        make_rank_predict_with_vrius(prefix, logger)                
        gc.collect()
        
    task = Task('make_cami2_patmg_short_rank_pred_with_virus', lambda u: f(u))
    task() 


def make_cami2_patmg_assembly_rank_pred_with_virus(filename):    
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = filename.replace(".fasta.gz", "")
            prefix = '{}_full_l{}'.format(prefix, length)            
            make_rank_predict_with_vrius(prefix, logger)

    task = Task('make_cami2_patmg_assembly_rank_pred_with_virus-{}'.format(filename), lambda u: f(u))
    task() 

            
def assembly_seq_full_predict_merge_with_vrius(prefix, seqlenfile, logger):               
        
    if True:
        outpath = os.path.join(config.PRED_PATH, "{}.allpred.rank.merged.gz".format(prefix))
        if os.path.exists(outpath):
            logger.info("skip " + outpath)
            return 

        files = [os.path.join(config.PRED_PATH, '{}_l{}.allpred.rank.gz'.format(prefix, length)) for length in SEQ_GSA_LENGTHS ]
        for filename in files:
            assert os.path.exists(filename), filename
        
        seqlen = pd.read_csv(seqlenfile, index_col=0)
        
        retlist = []
        for length in SEQ_GSA_LENGTHS:
            filepath = os.path.join(config.PRED_PATH, '{}_l{}.allpred.rank.gz'.format(prefix, length))
            pred = pd.read_csv(filepath, index_col=0)
            if length == 150:
                a, b = 0, 400
            elif length == 500:
                a, b = 400, 1100
            elif length == 1000:
                a, b = 1100, 3500
            elif length == 3000:
                a, b = 3500, np.inf
            index = seqlen[(seqlen['seqlen'] >= a) & (seqlen['seqlen'] < b)].index
            retlist.append(pred.loc[index])
        
        df = pd.concat(retlist).sort_index()
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, "{}.allpred.rank.merged.gz".format(prefix))
        logger.info("writing " + tmpoutpath)
        df.to_csv(tmpoutpath, compression='gzip')
        shutil.move(tmpoutpath, outpath)

        
def predict_cami2_patmg_full_with_virus_merge_task(filename):               
        
    def f(logger):
        prefix = filename.replace(".fasta.gz", '') 
        seqlenfile = os.path.join(config.SEQLEN_PATH, "{}.length.gz".format(prefix))
        assembly_seq_full_predict_merge_with_vrius(prefix + "_full", seqlenfile, logger)

    task = Task('predict_cami2_patmg_full_with_virus_merge_task-{}'.format(filename), lambda u: f(u))
    task() 

    
def run():
    make_cami2_patmg_short_rank_pred_with_virus()
    files = ['patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']
    for filename in files:
        make_cami2_patmg_assembly_rank_pred_with_virus(filename)
        predict_cami2_patmg_full_with_virus_merge_task(filename)
        
           
if __name__ == '__main__':
    run()
