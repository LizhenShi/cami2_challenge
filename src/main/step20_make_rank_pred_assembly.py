import config
import utils
from task import Task
import os
import pandas as pd 
from myzstd import ZstdFile
from joblib import Parallel, delayed
from tqdm import tqdm as tqdm
from utils import check_output_file_exists
from helper import BinningHelper
import shutil

from utils import get_input_file
from step12f_assembly_seq_predict_full import SEQ_GSA_LENGTHS


def get_id_to_sk():
    import json
    with open(os.path.join(config.INFO_PATH, 'gb_id_to_superkingdom.json'), 'rt') as fin:
        id_to_sk = json.load(fin)
    id_to_sk = {int(k):v for k, v in id_to_sk.items()}
    return id_to_sk


def parse_prob_text_only_bacteria(text):
    if isinstance(text, str):
        global id_to_sk
        if 'id_to_sk' not in globals():
            id_to_sk = get_id_to_sk()        
        l = [u.split(':') for u in text.split()]
        a = [int(u[0]) for u in l]
        b = [float(u[1]) for u in l]
        d = {k:v for k, v in zip(a, b) if k in id_to_sk and id_to_sk[k] == 2 }
        return d
    return None


def parse_prob_text(text):
    if isinstance(text, str):
        global id_to_sk
        if 'id_to_sk' not in globals():
            id_to_sk = get_id_to_sk()        
        l = [u.split(':') for u in text.split()]
        a = [int(u[0]) for u in l]
        b = [float(u[1]) for u in l]
        d = {k:v for k, v in zip(a, b) if k in id_to_sk and id_to_sk[k] in {2, 10239} }
        return d
    return None


def read_pred(fname, only_bacteria):
    lines = []
    with ZstdFile(fname, 'r') as fin:
        for line in fin:
            lines.append(line.split("\t"))

    pred = pd.DataFrame(lines)
    pred.columns = ['readid', 'text']
    if 'id_to_sk' in globals(): del id_to_sk
    if only_bacteria:
        pred['prob'] = Parallel(n_jobs=utils.get_num_thread())(delayed(parse_prob_text_only_bacteria)(u) for u in tqdm(pred['text'].values))
    else:
        pred['prob'] = Parallel(n_jobs=utils.get_num_thread())(delayed(parse_prob_text)(u) for u in tqdm(pred['text'].values))
    pred = pred.drop('text', axis=1)
    print (fname, pred.shape)
    return pred.set_index('readid')


def find_best_rank(u):
    global target_rank, binhelper
    maxp = -1
    maxr = -1
    for k, v in u.items():
        if binhelper.is_in_rank(k, target_rank):
            if maxp < v:
                maxp = v
                maxr = k
    return maxr


def make_rank_predict(prefix, logger, only_bacteria=True):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return

        fname = get_input_file(os.path.join(config.PRED_PATH, '{}.pred'.format(prefix)), logger)
        logger.info("reading prediction from " + fname)
        pred = read_pred(fname, only_bacteria)
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))

        global target_rank, binhelper
        binhelper = BinningHelper()
        RANKS = binhelper.ranks
        for target_rank in RANKS:
            logger.info("to find best rank pred for " + target_rank)
            pred[target_rank] = utils.parallell_for(pred['prob'].values, find_best_rank, n_jobs=utils.get_num_thread())
            pred[target_rank].value_counts().head(10)
        
        pred = pred[RANKS]
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))
        
        df = pred
        tmpoutpath = os.path.join(config.LOCAL_PATH, "{}.pred.rank.gz".format(prefix))
        logger.info("writing " + tmpoutpath)
        df.to_csv(tmpoutpath, compression='gzip')
        outpath = os.path.join(config.PRED_PATH, "{}.pred.rank.gz".format(prefix))
        shutil.move(tmpoutpath, outpath)
        

def make_rank_pred_cami2_toy_airway_gsa(fileno):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = 'CAMI_Airways_sample_{}_gsa_full_l{}'.format(fileno, length)
            make_rank_predict(prefix, logger)
                
    task = Task('make_rank_pred_cami2_toy_airway_gsa-{}'.format(fileno), lambda u: f(u))
    task() 


def make_rank_pred_cami2_toy_mousegut_gsa(fileno):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = 'CAMI_MOUSEGUT_sample_{}_gsa_full_l{}'.format(fileno, length)
            make_rank_predict(prefix, logger)

    task = Task('make_rank_pred_cami2_toy_mousegut_gsa-{}'.format(fileno), lambda u: f(u))
    task() 


def make_rank_pred_cami2_4_assembly(filename):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = filename.replace(".fasta.gz", "")
            prefix = '{}_full_l{}'.format(prefix, length)            
            make_rank_predict(prefix, logger)

    task = Task('make_rank_pred_cami2_4_assembly-{}'.format(filename), lambda u: f(u))
    task() 
            
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        toy_airway_nos = [4, 7, 10, 8, 9, 10, 11, 12, 23, 26, 27][:3]
        toy_mousegut_nos = [14, 15, 30, 31, 3, 46, 52, 53, 60, 8][:3]
        
        for no in toy_airway_nos:
            make_rank_pred_cami2_toy_airway_gsa(no)

        for no in toy_mousegut_nos:
            make_rank_pred_cami2_toy_mousegut_gsa(no)
    else:
        files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
                 'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz',
                  'patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']        
        for filename in files:
            make_rank_pred_cami2_4_assembly(filename)

        
if __name__ == '__main__':
    run()

