import config
import utils
from task import Task
import os
import gc

from utils import check_output_file_exists
from utils import get_input_file
from helper import BinningHelper
from step20_make_rank_pred_assembly import   \
    parse_prob_text_only_bacteria, parse_prob_text
from myzstd import ZstdFile
import pandas as pd 
from joblib import Parallel, delayed
from tqdm import tqdm 
import shutil
import sys


def find_best_rank(u):
    global target_rank, binhelper
    maxp = -1
    maxr = -1
    for k, v in u.items():
        if binhelper.is_in_rank(k, target_rank):
            if maxp < v:
                maxp = v
                maxr = k
    return maxr


def process_one_batch(lines, only_bacteria, logger):

    def _to_df():
        pred = pd.DataFrame(lines)
        pred.columns = ['readid', 'text']
        if 'id_to_sk' in globals(): del id_to_sk
        if only_bacteria:
            pred['prob'] = Parallel(n_jobs=utils.get_num_thread())(delayed(parse_prob_text_only_bacteria)(u) for u in tqdm(pred['text'].values))
        else:
            pred['prob'] = Parallel(n_jobs=utils.get_num_thread())(delayed(parse_prob_text)(u) for u in tqdm(pred['text'].values))
        pred = pred.drop('text', axis=1)
        return pred.set_index('readid')
    
    pred = _to_df()
    logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))

    global target_rank, binhelper
    RANKS = binhelper.ranks
    for target_rank in RANKS:
        logger.info("to find best rank pred for " + target_rank)
        pred[target_rank] = utils.parallell_for(pred['prob'].values, find_best_rank, n_jobs=min(20, utils.get_num_thread()))
    
    pred = pred[RANKS]
    logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))
    
    return pred
    
    
def read_pred(fname, only_bacteria, logger):
    global target_rank, binhelper
    binhelper = BinningHelper()
    RANKS = binhelper.ranks

    predlist = []
    
    lines = []
    with ZstdFile(fname, 'r') as fin:
        for line in fin:
            lines.append(line.split("\t"))
            if len(lines) >= 1000000:
                predlist.append(process_one_batch(lines, only_bacteria, logger))
                lines = []
                
    if len(lines) > 0:
        predlist.append(process_one_batch(lines, only_bacteria, logger))
        lines = []

    return pd.concat(predlist, axis=0)


def make_rank_predict_with_batch(prefix, logger, only_bacteria=True):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return

        fname = get_input_file(os.path.join(config.PRED_PATH, '{}.pred'.format(prefix)), logger)
        logger.info("reading prediction from " + fname)
                
        df = read_pred(fname, only_bacteria, logger)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, "{}.pred.rank.gz".format(prefix))
        logger.info("writing " + tmpoutpath)
        df.to_csv(tmpoutpath, compression='gzip')
        outpath = os.path.join(config.PRED_PATH, "{}.pred.rank.gz".format(prefix))
        shutil.move(tmpoutpath, outpath)


def predict_cami2_illumina(prefix, logger):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred'.format(prefix)), logger):
            return
        
        inputModel = os.path.join(config.MODEL_PATH, 'genbank_model_ill_k23', 'model_299')
        assert utils.file_exists(inputModel), inputModel

        hashfile = os.path.join(config.LSH_PATH, 'lsh_CAMI2_illumina_k23_h25.crp')
        assert utils.file_exists(hashfile), hashfile

        jarpath = os.path.join(config.HOME, "lib", "jfastseq-*-*.jar")
        
        filepath = get_input_file(os.path.join(config.SEQ_PATH, '{}.seq'.format(prefix)), logger)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, '{}.pred.zst'.format(prefix))
        
        n_thread = utils.get_num_thread()
        cmd = "java -server -Xmx{} -XX:+AggressiveOpts -cp {} net.jfastseq.Main predict --hash {} --input {} --output {} --inputModel {} --max {} --threshold {} --withoutUncult --thread {} ".format(
        utils.get_java_memory(n_thread),
        jarpath,
        hashfile,
        filepath,
        tmpoutpath,
        inputModel,
        500,
        0.005,
        n_thread)
        
        status = utils.shell_run_and_wait2(cmd)
        if status != 0:
            utils.remove_if_file_exit(tmpoutpath)
            msg = "Run command failed: " + cmd
            logger.error(msg)
            raise Exception(msg)
        
        outpath = os.path.join(config.PRED_PATH, '{}.pred.zst'.format(prefix))
        utils.move(tmpoutpath, outpath)
        return outpath


def predict_cami2_marmg_short(fileno):               
        
    def f(logger):
        prefix = 'marmgCAMI2_short_read_sample_{}_reads'.format(fileno)
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return        
        predict_cami2_illumina(prefix, logger)
        make_rank_predict_with_batch(prefix, logger, only_bacteria=True)                
        gc.collect()
                
    task = Task('predict_cami2_marmg_short-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_toy_airway_illumina(fileno):               
        
    def f(logger):
        prefix = 'CAMI_Airways_illumina_sample_{}'.format(fileno)
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return        
        predict_cami2_illumina(prefix, logger)
        make_rank_predict_with_batch(prefix, logger, only_bacteria=True)                
        gc.collect()

    task = Task('predict_cami2_toy_airway_illumina-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_toy_mousegut_illumina(fileno):               
        
    def f(logger):
        prefix = 'MOUSEGUT_illumina_sample_{}'.format(fileno)
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return        
        predict_cami2_illumina(prefix, logger)
        make_rank_predict_with_batch(prefix, logger, only_bacteria=True)
        gc.collect()
                
    task = Task('predict_cami2_toy_mousegut_illumina-{}'.format(fileno), lambda u: f(u))
    task() 

        
def run():
    isDebug = config.is_debug()
    
    if not isDebug:
        marmg_nos = [0] if isDebug else range(10)
        for i in marmg_nos:
            # cami2_marmg_long_to_seq(i)
            pass
            
        for i in marmg_nos:
            predict_cami2_marmg_short(i)        
    else:
        if isDebug:
            toy_airway_nos = [4, 7, 10, 8, 9, 11, 12, 23, 26, 27][:3]
            for i in toy_airway_nos:
                predict_cami2_toy_airway_illumina(i)
    
        if isDebug:
            toy_mousegut_nos = [14, 15, 30, 31, 3, 46, 52, 53, 60, 8][:2]
            for i in toy_mousegut_nos:
                predict_cami2_toy_mousegut_illumina(i)

            
if __name__ == '__main__':
    if len(sys.argv) == 1:
        run()
    elif len(sys.argv) == 3:
        if sys.argv[1] == 'marmg':
            fileno = int(sys.argv[2])
            predict_cami2_marmg_short(fileno)
        if sys.argv[1] == 'mousegut':
            fileno = int(sys.argv[2])
            predict_cami2_toy_mousegut_illumina(fileno)            
    else:
        raise Exception("parse arg failed")

