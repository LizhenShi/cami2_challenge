import config
import utils
from task import Task
import os
import pandas as pd 
from joblib import Parallel, delayed
from tqdm import tqdm as tqdm
from utils import check_output_file_exists
import shutil

from utils import get_input_file
from step20_make_rank_pred_assembly import parse_prob_text, read_pred, find_best_rank, make_rank_predict
        

def make_rank_pred_cami2_toy_airway_pacbio(sample_no):               
        
    def f(logger):
        prefix = 'CAMI_Airways_pacbio_sample_{}'.format(sample_no)
        make_rank_predict(prefix, logger)
                
    task = Task('make_rank_pred_cami2_toy_airway_pacbio-{}'.format(sample_no), lambda u: f(u))
    task() 


def make_rank_pred_cami2_toy_mousegut_pacbio(sample_no):               
        
    def f(logger):
        prefix = 'MOUSEGUT_pacbio_sample_{}'.format(sample_no)
        make_rank_predict(prefix, logger)

    task = Task('make_rank_pred_cami2_toy_mousegut_pacbio-{}'.format(sample_no), lambda u: f(u))
    task() 


def make_rank_pred_cami2_strmg_pacbio(sample_no):               
        
    def f(logger):
        prefix = 'strmgCAMI2_long_read_sample_{}_reads'.format(sample_no)
        make_rank_predict(prefix, logger)

    task = Task('make_rank_pred_cami2_strmad_pacbio-{}'.format(sample_no), lambda u: f(u))
    task() 


def make_rank_pred_cami2_marmg_pacbio(sample_no):               
        
    def f(logger):
        prefix = 'marmgCAMI2_long_read_sample_{}_reads'.format(sample_no)
        make_rank_predict(prefix, logger)

    task = Task('make_rank_pred_cami2_marmg_pacbio-{}'.format(sample_no), lambda u: f(u))
    task()     
    
        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [4, 7, 10]:
            make_rank_pred_cami2_toy_airway_pacbio(no)

        for no in [14, 15, 30]:
            make_rank_pred_cami2_toy_mousegut_pacbio(no)
    else:
        
        marmg_nos = [0] if isDebug else range(10)
        for i in marmg_nos:
            make_rank_pred_cami2_marmg_pacbio(i)
                
        mad_nos = [0] if isDebug else range(100)
        for i in mad_nos:
            make_rank_pred_cami2_strmg_pacbio(i)
            
                
if __name__ == '__main__':
    run()

