import config
from task import Task
import os
import pandas as pd 
import numpy as np 
import shutil
from strmghelper import StrmgTaxonomicProfiling, StrmgTaxonomicHelper,\
    StrmgBinningHelper

def make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold, logger, seqlenfile=None, is_illumina=False, baserank='strain'):               

    def groupby_df_by_rank(df, rank):
        df = df.copy()
        df[rank] = df['ranks'].map(lambda u: u[rank])
        df = df.groupby(rank)[['count']].sum()
        df['PERCENTAGE'] = df['count'] / df['count'].sum() * 100
        return df.sort_values('count', ascending=False)

    def make_profile(df, rank):
        df = groupby_df_by_rank(df, rank)
        df['TAXID'] = df.index.map(lambda u: binhelper.get_tax_bin_id(u, rank))
        df['RANK'] = rank
        df['abc'] = df['TAXID'].map(lambda u: helper.get_taxpath(u))
        df['RANK'] = df['abc'].map(lambda u: u[0])
        df['TAXPATH'] = df['abc'].map(lambda u: u[1])
        df['TAXPATHSN'] = df['abc'].map(lambda u: u[2])
        return df['TAXID    RANK    TAXPATH    TAXPATHSN    PERCENTAGE'.split()]
        
    if True:
        if os.path.exists(outputFile):
            print ("output existing, skip: " + outputFile)
            return 
        
        if not os.path.exists(os.path.join(rankPredFile)):
            raise Exception (rankPredFile + " does not found")

        pred = pd.read_csv(rankPredFile, index_col=0)
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))
        logger.info("pred['superkingdom'].value_counts():\n{}".format(pred['superkingdom'].value_counts()))

        if is_illumina: 
            pred['seqlen'] = 150
        else:
            readlen = pd.read_csv(seqlenfile , index_col=0)
            pred['seqlen'] = readlen['seqlen']

        logger.info("rank==-1:\n{}".format((pred == -1).mean()))
        
        for u in pred.columns:
            if u != 'seqlen':
                print ('-1 percentage:', u, pred.loc[ pred[u] == -1, 'seqlen'].sum() / pred['seqlen'].sum())

        RANKS = ['superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'strain']

        profiler = StrmgTaxonomicProfiling()
        helper = StrmgTaxonomicHelper()
        binhelper = StrmgBinningHelper()
        
        profiler.set_comment("tax profile based on rank prediction")
        profiler.set_sample_id(sample_id)
        
        for rank in [baserank]:
            assert rank in ['strain']
            df = pred[[rank, 'seqlen']]
            df = df[df[rank] > 0] 
            df = df.groupby(rank).sum().reset_index()
            df.index = df[rank].values
            df['taxlen'] = df.index.map(lambda u: helper.get_length(u))
            df['count'] = df['seqlen'] / df['taxlen']
            
            df['ranks'] = df.index.map(lambda u:binhelper.get_ranks(u))
            if threshold and threshold > 0:
                maxcount = df['count'].max()
                df = df[df['count'] >= maxcount * threshold]
            df['PERCENTAGE'] = df['count'] / df['count'].sum() * 100
            df = df.sort_values('count', ascending=False)
            straindf = df

        retlist = []
        for u in RANKS:
            retlist.append(make_profile(straindf, u))

        retdf = pd.concat(retlist)
        retdf.loc[retdf['TAXID'].astype(np.int)>=3000000,'TAXID'] = retdf.loc[retdf['TAXID'].astype(np.int)>=3000000, 'TAXPATH'].map(lambda u: u.split('|')[-1])

        for row in retdf.values:
            profiler.add(*row)
            
        logger.info("writing " + outputFile + ".tmp")
        profiler.write(outputFile + ".tmp")
        shutil.move(outputFile + ".tmp", outputFile) 

def make_taxprof_submission_cami2_assembly_strmg_gsa(filename, baserank, threshold):               

    def f(logger):
        prefix = filename.replace(".fasta.gz", '') 
        if 1:
            sample_id = "{}".format(prefix)
            outputFile = os.path.join(config.SUB_PATH, "{}_strmg.profile.{}.th{}.txt.txt".format(prefix, baserank, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, '{}_full_strmg.pred.rank.merged.gz'.format(prefix))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "{}.length.gz".format(prefix))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False, baserank="strain")
                
    task = Task('make_taxprof_submission_cami2_assembly_strmg_gsa-{}-{}-{}'.format(filename, baserank, threshold), lambda u: f(u))
    task() 
        
 
if __name__ == '__main__':
    pass
