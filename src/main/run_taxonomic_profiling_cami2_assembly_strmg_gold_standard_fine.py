#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''
from step11f_assembly_to_seq_full import cami2_gsa_to_seq_full
from step12c_make_assembly_seq_length import make_assembly_seq_length
from step12g_assembly_strmg_seq_predict_full import predict_cami2_strmg_gsa_full_task
from step21g_assembly_strmg_seq_rank_predict_merge import predict_cami2_strmg_gsa_full_merge_task
import config
from step23g_make_taxprofile_submission_assembly_strmg import make_taxprof_submission_cami2_assembly_strmg_gsa
from step20g_make_rank_pred_assembly import make_rank_pred_cami2_4_assembly_strmg

 
def run(filename):

    cami2_gsa_to_seq_full(filename)

    make_assembly_seq_length(filename.replace('.fasta.gz', ""))
    
    prefix = filename.replace(".fasta.gz", '_full_strmg')
    seqprefix = filename.replace(".fasta.gz", '_full')
    predict_cami2_strmg_gsa_full_task(prefix, seqprefix)
    
    make_rank_pred_cami2_4_assembly_strmg(filename)
    
    predict_cami2_strmg_gsa_full_merge_task(filename) 
    
    for threshold in [0.3]:
        make_taxprof_submission_cami2_assembly_strmg_gsa(filename, "strain", threshold)

    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))

    
if __name__ == '__main__':
    filename = 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz'    
    run(filename)
