import config
import utils
from task import Task
import os
import pandas as pd 
import numpy as np 

from myzstd import ZstdFile
import tempfile
import glob
from tqdm import tqdm 
from joblib import Parallel, delayed

from utils import check_output_file_exists
from utils import get_input_file
from step12_assembly_seq_predict import SEQ_GSA_LENGTHS


def parse_prob(b):
    l = [u.split(':') for u in b.split()]
    l = [(int(u[0]), float(u[1])) for u in l]
    return pd.Series(dict(l)).astype(np.float32) 


def reduce_prob(subdf):
    count = subdf['count'].sum()
    probs = subdf['prob'].values
    if len(probs) == 1:
        return count, probs[0]
    else:
        probdf = pd.concat(probs, axis=1).fillna(0)
        count = subdf['count'].sum()
        return count, probdf.sum(axis=1).astype(np.float32)


def reduce_df(df):
    newdf = df.groupby('rid').apply(reduce_prob).to_frame()
    newdf ['count'] = newdf[0].map(lambda u: u[0])
    newdf ['prob'] = newdf[0].map(lambda u: u[1])
    return newdf[['count', 'prob']]


def process_one_file(fpath):
    df = pd.read_csv(fpath, header=None, sep="\t")
    df.columns = ['rid', 'prob']
    df['count'] = 1
    df['prob'] = df['prob'].map(parse_prob)
    newdf = df.groupby('rid').apply(reduce_prob).to_frame()
    newdf ['count'] = newdf[0].map(lambda u: u[0])
    newdf ['prob'] = newdf[0].map(lambda u: u[1])
    return newdf[['count', 'prob']]


def prob_to_text(s):
    s = (s[1] / s[0])
    s=s[s>=0.005]
    d=s.to_dict()
    return  " ".join(["{}:{:.6f}".format(k, v) for k, v in d.items()])


def merge_cami2_gsa_prediction(prefix, length, logger):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.merged'.format(prefix)), logger):
            return
        
        filepath = get_input_file(os.path.join(config.PRED_PATH, '{}.pred'.format(prefix)), logger)
        
        with tempfile.TemporaryDirectory() as tmpdirname:
            cmd = "cd {} && cat {} | zstd -d | split -C 32M --additional-suffix .txt - data.".format(tmpdirname, filepath)
            status = utils.shell_run_and_wait2(cmd)
            if status != 0:
                msg = "Run command failed: " + cmd
                logger.error(msg)
                raise Exception(msg)
            
            files = glob.glob('{}/data.*.txt'.format(tmpdirname))
            logger.info("processing {} files".format(len(files)))
            dflist = Parallel(n_jobs=utils.get_num_thread())(delayed(process_one_file)(v) for v in tqdm(files))
        
        logger.info("merging {} dataframes".format(len(dflist)))
        df = pd.concat(dflist).reset_index()
        df = reduce_df(df)
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        
        logger.info("making prob to text")
        text = Parallel(n_jobs=utils.get_num_thread())(delayed(prob_to_text)(v) for v in tqdm(df[['count', 'prob']].values))
        df['text'] = text
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
                
        tmpoutpath = os.path.join(config.LOCAL_PATH, '{}.pred.merged.zst'.format(prefix))
        logger.info("writing " + tmpoutpath)
        with ZstdFile(tmpoutpath, 'w') as fout:
            df[['text']].to_csv(fout, sep="\t", header=None)
        
        outpath = os.path.join(config.PRED_PATH, '{}.pred.merged.zst'.format(prefix))
        utils.move(tmpoutpath, outpath)


def merge_cami2_toy_airway_gsa_prediction(fileno):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = 'CAMI_Airways_sample_{}_gsa_l{}'.format(fileno, length)
            merge_cami2_gsa_prediction(prefix, length, logger)
                
    task = Task('merge_cami2_toy_airway_gsa_prediction-{}'.format(fileno), lambda u: f(u))
    task() 

def merge_cami2_toy_mousegut_gsa_prediction(fileno):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = 'CAMI_MOUSEGUT_sample_{}_gsa_l{}'.format(fileno, length)
            merge_cami2_gsa_prediction(prefix, length, logger)
                
    task = Task('merge_cami2_toy_mousegut_gsa_prediction-{}'.format(fileno), lambda u: f(u))
    task() 
        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [10]:
            merge_cami2_toy_airway_gsa_prediction(no)

        for no in [14]:
            merge_cami2_toy_mousegut_gsa_prediction(no)
            
    files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
             'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz']
    for filename in files:
        cami2_gsa_to_seq(filename)

        
if __name__ == '__main__':
    run()

