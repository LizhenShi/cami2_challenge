import config
import utils
from task import Task
import os
import sys
from utils import check_output_file_exists
    
def cami2_marmg_long_to_seq(fileno):               

    def f(logger):
        
        if check_output_file_exists(os.path.join(config.SEQ_PATH, 'marmgCAMI2_long_read_sample_{}_reads.seq'.format(fileno)), logger):
            return
        
        filepath = os.path.join(config.RAW_PATH, 'marmgCAMI2_long_read_sample_{}_reads.fq.gz'.format(fileno))
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, 'marmgCAMI2_long_read_sample_{}_reads.seq.zst'.format(fileno))
        
        cmd = 'python {}/src/main/fastqToSeq.py -i {} -o {} --cami2'.format(config.HOME, filepath, tmpoutpath)
        status = utils.shell_run_and_wait(cmd)
        if status != 0:
            utils.remove_if_file_exit(tmpoutpath)
            msg = "Run command failed: " + cmd
            logger.error(msg)
            raise Exception(msg)
        
        outpath = os.path.join(config.SEQ_PATH, 'marmgCAMI2_long_read_sample_{}_reads.seq.zst'.format(fileno))
        utils.move(tmpoutpath, outpath)
       
    task = Task('cami2_marmg_long_to_seq-{}'.format(fileno), lambda u: f(u))
    task()    


def cami2_marmg_short_to_seq(fileno):               
        
    def f(logger):
        if check_output_file_exists(os.path.join(config.SEQ_PATH, 'marmgCAMI2_short_read_sample_{}_reads.seq'.format(fileno)), logger):
            return
        
        filepath = os.path.join(config.RAW_PATH, 'marmgCAMI2_short_read_sample_{}_reads.fq.gz'.format(fileno))
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, 'marmgCAMI2_short_read_sample_{}_reads.seq.zst'.format(fileno))
        
        cmd = 'python {}/src/main/fastqToSeq.py -i {} -o {} --cami2 -p 1'.format(config.HOME, filepath, tmpoutpath)
        status = utils.shell_run_and_wait(cmd)
        if status != 0:
            utils.remove_if_file_exit(tmpoutpath)
            msg = "Run command failed: " + cmd
            logger.error(msg)
            raise Exception(msg)
        
        outpath = os.path.join(config.SEQ_PATH, 'marmgCAMI2_short_read_sample_{}_reads.seq.zst'.format(fileno))
        utils.move(tmpoutpath, outpath)
        
    task = Task('cami2_marmg_short_to_seq-{}'.format(fileno), lambda u: f(u))
    task() 


    
def run():
    isDebug = config.is_debug()
    
    marmg_nos = [0] if isDebug else range(10)
    for i in marmg_nos:
        cami2_marmg_long_to_seq(i)
        
    for i in marmg_nos:
        cami2_marmg_short_to_seq(i)        
        
    mad_nos = [0] if isDebug else range(100)

if __name__ == '__main__':
    run()

