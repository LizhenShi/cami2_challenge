#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
from step1_fastq_to_seq_strmg import cami2_strmg_long_to_seq
from step2_predict_long_strmg import make_seq_length_strmg_long, \
    predict_cami2_strmg_long
from step20_make_rank_pred_pacbio import make_rank_pred_cami2_strmg_pacbio
from step22_make_binning_submission_pacbio import make_binning_submission_cami2_strmg_pacbio
 

def run(sample_no):
    cami2_strmg_long_to_seq(sample_no)
    make_seq_length_strmg_long(sample_no)
    predict_cami2_strmg_long(sample_no)
    
    make_rank_pred_cami2_strmg_pacbio(sample_no)

    make_binning_submission_cami2_strmg_pacbio(sample_no)

    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))

        
def main():
    for sample_no in range(100):
        run(sample_no)
            
            
if __name__ == '__main__':
    main()
