'''
Created on Jul 12, 2019

@author: bo
'''

import config
import sys 
import os
import numpy as np 
import pandas as pd 
import gzip


class TaxonomicHelper(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        pass 
    
    def __read_tax_profile_info(self):
            filepath = os.path.join(config.INFO_PATH, 'genbank_tax_profile_info.csv.gz')
            df = pd.read_csv(filepath, dtype={'TAXID' :'str'})
            df['TAXID'] = df['TAXID'].map(lambda u: u.split(".")[0] if u.endswith('.0') else u)
            df = df.set_index('TAXID')
            return df
        
    def set_if_not_exists(self, name, fun):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            # print ("call fun for " + name)
            value = fun()
            setattr(self, name, value)
            return value 
    
    @property
    def id2rank(self):

        def f():
            df = self.__read_tax_profile_info()
            d = df['RANK'].to_dict()
            return d

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)

    @property
    def id2taxpath(self):

        def f():
            df = self.__read_tax_profile_info()
            return df.apply(lambda u: tuple(u), axis=1).to_dict()

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)
                
    @property
    def id2length(self):

        def f():
            df = self.__read_tax_profile_info()
            
            return df['TAXLEN'].to_dict()

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)
                
    def get_rank(self, taxid):
        taxid = str(taxid) 
        return self.id2rank[taxid] if taxid in self.id2rank else None 
    
    def get_length(self, taxid):
        taxid = str(taxid) 
        return self.id2length[taxid] if taxid in self.id2length else None 
    
    def get_taxpath(self, taxid):
        taxid = str(taxid)
        return self.id2taxpath[taxid] if taxid in self.id2taxpath else None 


class BinningHelper(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.ranks = "superkingdom,phylum,class,order,family,genus,species,strain".split(",")
        self.rank_suffix = {v:k + 1 for k, v in enumerate(self.ranks)} 
        self.taxhelper = TaxonomicHelper()
    
    def set_if_not_exists(self, name, fun):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            # print ("call fun for " + name)
            value = fun()
            setattr(self, name, value)
            return value 
    
    @property
    def __id2ranks(self):

        def f():
            filepath = os.path.join(config.INFO_PATH, 'genbank_tax_binning_info.csv.gz')
            df = pd.read_csv(filepath, index_col=0)
            d = {}
            for rank in self.ranks:
                d[rank] = df[rank].to_dict()
            return d

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)

    @property
    def __genusid2ranks(self):

        def f():
            filepath = os.path.join(config.INFO_PATH, 'genbank_tax_binning_info.csv.gz')
            df = pd.read_csv(filepath, index_col=0)
            df = df.drop(['species', 'strain'], axis=1)
            df = df.drop_duplicates() 
            df.index = df['genus'].values
            d = {}
            for rank in self.ranks:
                if rank not in ['species', 'strain']:
                    d[rank] = df[rank].to_dict()
            return d

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)

    @property
    def __indexes(self):

        def f():
            filepath = os.path.join(config.INFO_PATH, 'genbank_tax_binning_info.csv.gz')
            df = pd.read_csv(filepath, index_col=0)
            return set(df.index)

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)
        
    @property
    def __ranksSet(self):

        def f():
            d = self.__id2ranks
            ret = {}
            for rank in self.ranks:
                ret[rank] = set(d[rank].values())
            return ret

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)
    
    def is_in_whole(self, taxid):
        return taxid in self.__indexes
    
    def is_in_rank(self, taxid, rank):
        return taxid in self.__ranksSet[rank]
    
    def get_tax_bin_id(self, taxid, rank):
        if self.taxhelper.get_rank(taxid) == rank:
            return str(taxid)
        else:
            return "{}.{}".format(taxid, self.rank_suffix[rank])
    
    def get_ranks(self, taxid):
        ret = {}
        for rank in self.ranks:
            ret[rank] = self.__id2ranks[rank][taxid]
        return ret 

    def get_genus_ranks(self, taxid):
        ret = {}
        for rank in self.ranks:
            if rank not in ['species', 'strain']:
                ret[rank] = self.__genusid2ranks[rank][taxid]
        return ret 

         
class Binning():
    
    def __init__(self, has_taxid=True, has_binid=False, has_seqlen=False):
        
        assert has_taxid or has_binid
        
        self.comment = 'Format for Binning'
        self.version = '0.9.0'
        
        self.sample_id = "SAMPLEID"
        
        self.records = []
        
        self.has_taxid = has_taxid 
        self.has_binid = has_binid
        self.has_seqlen = has_seqlen
        
    def set_comment(self, txt):
        self.comment = txt
        
    def set_version(self, ver):
        self.version = ver 
        
    def set_sample_id(self, sid):
        self.sample_id = sid 
        
    def add(self, seqid, taxid, binid=None, seqlen=None):
        self.records.append((seqid, taxid, binid, seqlen))
        
    def write(self, filepath):
        has_taxid = self.has_taxid
        has_binid = self.has_binid
        with open(filepath , 'wt') as fout:
            fout.write("#{}\n".format(self.comment))
            fout.write("@Version:{}\n".format(self.version))
            fout.write("@SampleID:{}\n".format(self.sample_id))
            if 1:
                tmp = ['SEQUENCEID']
                if has_taxid:
                    tmp.append("TAXID")
                if has_binid:
                    tmp.append("BINID")
                if self.has_seqlen:
                    tmp.append("_LENGTH")
                fout.write("@@" + "\t".join(tmp) + "\n")
                
            for r in self.records:
                
                if has_taxid and has_binid:
                    if self.has_seqlen:
                        fout.write("\t".join([str(u) for u in r]))
                    else:
                        fout.write("\t".join([str(u) for u in r[:3]]))
                elif has_taxid:
                    if self.has_seqlen:
                        fout.write("\t".join([str(r[u]) for u in [0, 1, 3]]))
                    else:
                        fout.write("\t".join([str(r[u]) for u in [0, 1]]))
                elif has_binid:
                    if self.has_seqlen:
                        fout.write("\t".join([str(r[u]) for u in [0, 2, 3]]))
                    else:
                        fout.write("\t".join([str(r[u]) for u in [0, 2]]))
                else:
                    raise Exception("should not be here")
                fout.write("\n")

    @staticmethod
    def read(filepath):
        lst = []
        is_gzip = filepath.endswith('gz')
        with gzip.open(filepath , 'rt') if is_gzip else open(filepath , 'rt') as fin:
            for line in fin:
                line = line.strip()
                if line:
                    if line[0] in {'#', '@'}:
                        pass
                    else:
                        lst.append(line.split("\t"))
        return lst


class TaxonomicProfiling():
    
    def __init__(self):
        
        self.comment = 'Format for TaxonomicProfiling'
        self.version = '0.9.0'
        
        self.sample_id = "SAMPLEID"
        self.ranks = 'superkingdom|phylum|class|order|family|genus|species|strain'
        
        self.records = []
        
    def set_comment(self, txt):
        self.comment = txt
        
    def set_version(self, ver):
        self.version = ver 
        
    def set_sample_id(self, sid):
        self.sample_id = sid 

    def set_ranks(self, ranks):
        self.ranks = ranks
            
    def add(self, taxid, rank, taxpath, taxpathsn, percentage):
        self.records.append((taxid, rank, taxpath, taxpathsn, percentage))
        
    def write(self, filepath):
        with open(filepath , 'wt') as fout:
            fout.write("#{}\n".format(self.comment))
            fout.write("@Version:{}\n".format(self.version))
            fout.write("@SampleID:{}\n".format(self.sample_id))
            fout.write("@Ranks:{}\n".format(self.ranks))
            fout.write('@@' + "\t".join ('TAXID    RANK    TAXPATH    TAXPATHSN    PERCENTAGE'.split()) + "\n")
            for r in self.records:
                fout.write("\t".join([str(u) for u in r]))
                fout.write("\n")

    @staticmethod
    def read(filepath):
        lst = []
        with open(filepath , 'rt') as fin:
            for line in fin:
                line = line.strip()
                if line:
                    if line[0] in {'#', '@'}:
                        pass
                    else:
                        lst.append(line.split("\t"))
        return lst

