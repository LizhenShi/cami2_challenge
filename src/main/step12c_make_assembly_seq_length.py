import config
import utils
from task import Task
import os
import pandas as pd 

from utils import get_input_file
from step11_assembly_to_seq import read_fasta

    
def make_assembly_seq_length(prefix):               
        
    def f(logger):
        outpath = os.path.join(config.SEQLEN_PATH, '{}.length.gz'.format(prefix))
        if os.path.exists(outpath):
            logger.info("skip " + outpath)
            return 

        inputpath = get_input_file(os.path.join(config.ASSEMBLY_PATH, prefix + ".fasta"), logger)
        
        sequences = read_fasta(inputpath)
        
        lst = [ [u, len(v)] for u, v in sequences]
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, '{}.length.gz'.format(prefix))
        df = pd.DataFrame(lst, columns=['readid', 'seqlen'])
        df[['readid', 'seqlen']].to_csv(tmpoutpath, index=None, compression='gzip')
        
        utils.move(tmpoutpath, outpath)
        
    task = Task('make_assembly_seq_length-{}'.format(prefix), lambda u: f(u))
    task()      

        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [4,7,10]:
            prefix = 'CAMI_Airways_sample_{}_gsa'.format(no)
            make_assembly_seq_length(prefix)

        for no in [14,15,30]:
            prefix = 'MOUSEGUT_sample_{}_gsa'.format(no)
            make_assembly_seq_length(prefix)
            
    files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
             'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz',
             'patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']
    for filename in files:
        make_assembly_seq_length(filename.replace('.fasta.gz', ""))

        
if __name__ == '__main__':
    run()

