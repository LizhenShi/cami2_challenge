#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''
import run_taxonomic_profiling_cami2_marmg_illumina_reads
from step24a_merge_taxprofile_marmg_submission import step24a_merge_taxprofile_marmg_submission_illumina
 

def main():
    for i in range(10):
        run_taxonomic_profiling_cami2_marmg_illumina_reads.run(i)
    step24a_merge_taxprofile_marmg_submission_illumina(5)

            
if __name__ == '__main__':
    main()
