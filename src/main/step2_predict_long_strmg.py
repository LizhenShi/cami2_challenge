import config
from task import Task

from step2_predict_long_marmg import predict_cami2_pacbio,\
    make_seq_length_pacbio

        
def predict_cami2_strmg_long(fileno):               
        
    def f(logger):
        prefix = 'strmgCAMI2_long_read_sample_{}_reads'.format(fileno)
        predict_cami2_pacbio(prefix, logger)
        
    task = Task('predict_cami2_strmg_long-{}'.format(fileno), lambda u: f(u))
    task() 

def make_seq_length_strmg_long(fileno):               
        
    def f(logger):
        prefix = 'strmgCAMI2_long_read_sample_{}_reads'.format(fileno)
        make_seq_length_pacbio(prefix, logger)
        
    task = Task('make_seq_length_strmg_long-{}'.format(fileno), lambda u: f(u))
    task()     


def run():
    isDebug = config.is_debug()
    
    mad_nos = [0] if isDebug else range(100)
        
    for i in mad_nos:
        make_seq_length_strmg_long(i)
        predict_cami2_strmg_long(i)
        
           
if __name__ == '__main__':
    run()

