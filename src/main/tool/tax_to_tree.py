'''
Created on Oct 4, 2019

@author: bo
'''
import os
import sys
import pandas as pd 
import gzip, json

class Node(object):

    def __init__(self, nid, rank, name):
        self.nid, self.rank, self.name = nid, rank, name
        self.children = set([])
        self.parent_id = None
        
    def set_if_not_exists(self, name, fun):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            # print ("call fun for " + name)
            value = fun()
            setattr(self, name, value)
            return value 
        
    def set_parent(self, pid):
        if self.nid == 1:
            pass
        elif self.parent_id is None or self.parent_id == pid:
            self.parent_id = pid
        else:
            raise Exception("ERROR2: {} {}".format(pid, self.parent_id))
        
    def is_root(self):
        return self.parent_id is None
    
    def is_child(self):
        return len(self.children) == 0
    
    def add_child(self, cid):
        if cid == 1 and self.nid == 1:
            pass
        else:
            assert cid != self.nid
            self.children.add(cid)
        
    def __repr__(self):
        return str([self.nid, self.rank, self.name, self.children, self.parent_id])

    
class Tree(object):

    def __init__(self):
        self.nodes = {}
        self.root = None
        
    def set_if_not_exists(self, name, fun):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            # print ("call fun for " + name)
            value = fun()
            setattr(self, name, value)
            return value 
        
    def add_node(self, nid, rank, name):
        node = Node(nid, rank, name)
        if node.is_root():
            if self.root is None and nid == 1:
                self.root = node
            else:
                pass
                # raise Exception("ERROR2: {} {}".format(nid,rank))
        self.nodes[node.nid] = node

    def get_node(self, nid):
        return self.nodes[nid]
    
    def get_parent(self, nid):
        return self.get_node(nid).parent_id
    
    def get_children(self, nid):
        return self.get_node(nid).children
    
    def valid(self):
        assert self.root is not None
        for nid in self.nodes:
            if nid != 1:
                assert self.get_parent(nid) is not None
        return True

    @property
    def nodePaths(self):

        def f():
            paths = {}
            fqnodesmap = self.nodes

            def get_path(nid):
                if nid in paths:
                    return paths[nid]
                node = fqnodesmap[nid]
                pid = node.parent_id
                if pid is None:
                    pass
                else:
                        
                    p = get_path(pid) + [pid]
                    paths[nid] = p
                    return p

            paths[1] = []        
            for nid in fqnodesmap:
                get_path(nid)
            node_paths = paths 
            return node_paths   
    
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)  
    
    def get_node_path(self, nid):
        return self.nodePaths[ (nid)]
        
    def get_filtered_nodes(self, filtered_node_id):
        if filtered_node_id is None:
            return set() 
        
        kept = list(self.get_node_path(filtered_node_id))
        kept.append(filtered_node_id)
        for k, v in self.nodePaths.items():
            if filtered_node_id in v:
                kept.append(k)
        return set(kept)
 
    def remove_node(self, nid):
        assert nid != self.root.nid
        node = self.get_node(nid)
        del self.nodes[nid]
        
        pid = node.parent_id
        assert nid != pid
        children = node.children
        assert pid in self.nodes, str([nid, pid])
        self.get_node(pid).children = {u for u in self.get_node(pid).children if u != nid}
        for cid in children:
            assert cid in self.nodes, str([nid, cid])
            self.get_node(cid).parent_id = pid
            self.get_node(pid).children.add(cid)
       
    def num_sub_nodes(self, nid):
        cnt = 0
        node = self.get_node(nid)
        children = node.children
        for cid in children:
            if cid != 1:
                cnt += 1
                cnt += self.num_sub_nodes(cid)
        return cnt            

    
class ncbi_taxonomy(object):

    def __init__(self, path, filtered_nodes=None):
        assert os.path.exists(path)
        self.filtered_nodes = filtered_nodes
        self.path = path
        self.ranks = set(['root', 'superkingdom', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species'])
    
    def set_if_not_exists(self, name, fun):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            # print ("call fun for " + name)
            value = fun()
            setattr(self, name, value)
            return value 
    
    @property
    def nodes(self):

        def f():
            filepath = os.path.join(self.path, 'nodes.dmp')
            nodes = pd.read_csv(filepath, sep='|', header=None)
            nodes.columns = ['tax_id', 'parent_id', 'rank', 'embl', 'division', 'dv_flag', 'gen_code_id', 'gc_flag', 'mit_code', 'mgc_code', 'genbank_hidflag', 'hid_flag', 'comments', 'na']
            for u in ['rank', 'comments', 'embl']:
                nodes[u] = nodes[u].map(lambda u: u.strip())
            
            if self.filtered_nodes is not None:
                nodes = nodes[nodes['tax_id'].isin(self.filtered_nodes)]
                nodes = nodes[nodes['parent_id'].isin(self.filtered_nodes)]
            return nodes
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)        

    @property
    def names(self):

        def f():
            filepath = os.path.join(self.path, 'names.dmp')
            names = pd.read_csv(filepath, sep='|', header=None)
            for i in range(1, 4):
                names[i] = names[i].map(lambda u: u.strip())
            names.columns = ['tax_id', 'name_text', 'unique_name', 'name_class', 'na']
            if self.filtered_nodes is not None:
                names = names[names['tax_id'].isin(self.filtered_nodes)]
            return names

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)        

    @property
    def mynames(self):

        def f():
            mynames = dict(self.names.loc[self.names['name_class'] == 'scientific name', ['tax_id', 'name_text']].values)
            return mynames

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)        
  
    def get_names(self, tax_id):
        return self.mynames[tax_id]

    @property
    def tree(self):

        def f():
            tree = Tree()
            nodes = self.nodes
            for a in nodes[['tax_id', 'parent_id', 'rank']].values:
                # print list(a)
                tree.add_node(a[0], a[2], self.get_names(a[0]))
            for a in nodes[['tax_id', 'parent_id']].values:
                # print list(a)
                node = tree.get_node(a[0])
                node.set_parent(a[1])
                node = tree.get_node(a[1])
                node.add_child(a[0])
            tree.root = tree.get_node(1)
            
            return tree

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)    

    @property
    def binaryTree(self):

        def f():
            global dummy_node
            dummy_node = 4000000
            global idx
            idx = 0
            allnodes = {}
            allfqnodes = []
            tree = self.tree
            
            def create_fqnode(nid, parent, left, right, count, binary, name=None, rank=None):
                return {u: v for u, v in locals().items()}
                        
            def create_dummynode(pid):
                global dummy_node
                dummynode1 = Node(nid=dummy_node, rank="dummy", name="dummy" + str(dummy_node))
                dummynode1.set_parent(pid)
                dummy_node += 1
                return dummynode1
            
            def search(nid, pid=None):
                nid=int(nid)
                global idx
                idx += 1
                # if idx>100: return
                node = allnodes[nid] if nid in allnodes else tree.nodes[nid]
                children = list(node.children)
                if(len(children) == 0):
                    # assert False, "should not be here"
                    fqnode = create_fqnode(nid, parent=pid, left=-1, right=-1, count=100, binary=False, rank=node.rank, name=node.name)
                    allfqnodes.append(fqnode)
            
                elif (len(children) == 1):
                    left = tree.nodes[children[0]]
                    # right=create_dummynode(nid)
                    
                    allnodes[left.nid] = left
                    # allnodes[right.nid]=right
                    
                    search(children[0], nid)
                    
                    fqnode = create_fqnode(nid, parent=pid, left=left.nid, right=-1, count=100, binary=True, rank=node.rank, name=node.name)
                    allfqnodes.append(fqnode)
            
                elif (len(children) == 2):   
                    left = tree.nodes[children[0]]
                    right = tree.nodes[children[1]]
                    
                    allnodes[left.nid] = left
                    allnodes[right.nid] = right
                    
                    search(children[0], nid)
                    search(children[1], nid)
            
                    fqnode = create_fqnode(nid, parent=pid, left=left.nid, right=right.nid, count=100, binary=True, rank=node.rank, name=node.name)
                    allnodes[left.nid] = left
                    allnodes[right.nid] = right
                    allfqnodes.append(fqnode)
            
                else:
                    k = int(len(children) / 2)
                    
                    dummynode1 = create_dummynode(nid)
                    for u in children[:k]: dummynode1.add_child(u)
            
                    dummynode2 = create_dummynode(nid)
                    for u in children[k:]: dummynode2.add_child(u)
                    allnodes[dummynode1.nid] = dummynode1
                    allnodes[dummynode2.nid] = dummynode2
            
                    search(dummynode1.nid, nid)
                    search(dummynode2.nid, nid)
                    
                    fqnode = create_fqnode(nid, parent=pid, left=dummynode1.nid, right=dummynode2.nid, count=100, binary=True, rank=node.rank, name=node.name)
                    allfqnodes.append(fqnode)        

            search(1)
            
            fqnodesmap = {u['nid']:u for u in allfqnodes}
            return fqnodesmap
            # fqnodesmap2 = {str(k):v for k, v in fqnodesmap.items()}
            # return fqnodesmap2
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)    

    def save(self, filepath):
        fqnodesmap2 = {str(k): v for k, v in self.binaryTree.items()}
        if filepath.endswith('.gz'):
            with gzip.open(filepath, 'wt') as fout:
                json.dump(fqnodesmap2, fout)
        else:
            with open(filepath, 'wt') as fout:
                json.dump(fqnodesmap2, fout)
    
    @property
    def nodePaths(self):

        def f():
            paths = {}
            fqnodesmap = self.binaryTree

            def get_path(nid):
                if nid in paths:
                    return paths[nid]
                node = fqnodesmap[nid]
                pid = node['parent']
                if pid is None:
                    pass
                else:
                    pnode = fqnodesmap[pid]
                    if nid == pnode['left']:
                        code = True
                    elif nid == pnode['right']:
                        code = False
                    else:
                        raise Exception("something wrong")
                        
                    p = get_path(pid) + [pid]
                    paths[nid] = p
                    return p

            paths[1] = []        
            for nid in fqnodesmap:
                get_path(nid)
            node_paths = paths 
            return node_paths   
    
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)  
        
    def get_node_path(self, nid):
        return self.nodePaths[ (nid)]
 
    @property
    def namePaths(self):

        def f():
            paths = {}
            fqnodesmap = self.binaryTree

            def get_path(nid):
                if nid in paths:
                    return paths[nid]
                node = fqnodesmap[nid]
                pid = node['parent']
                if pid is None:
                    pass
                else:
                    pnode = fqnodesmap[pid]
                    if nid == pnode['left']:
                        code = True
                    elif nid == pnode['right']:
                        code = False
                    else:
                        raise Exception("something wrong")
                        
                    p = get_path(pid) + [ pnode['name']]
                    paths[nid] = p
                    return p  
    
            paths[1] = []        
            for nid in fqnodesmap:
                get_path(nid)
            name_paths = paths  
            return name_paths

        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)  
        
    def get_name_path(self, nid):
        return self.namePaths[ (nid)] 


if __name__ == '__main__':
    path = '/home/bo/Downloads/taxdmp'

    def test_default():    
        tax = ncbi_taxonomy(path)
        
        print(tax.nodes.head())
        
        tree = tax.tree
        
        print(tree.valid())
        print (tree.num_sub_nodes(1))
        print (tree.num_sub_nodes(2))
        print (tree.num_sub_nodes(10239))
        
        print(tree.get_node_path(4530))
        print (tree.get_children(1))
        
        if 0:
            print(tax.get_node_path(4530))
            print(tax.get_name_path(4530))

    def test_Bacteria():    
        tax = ncbi_taxonomy(path)
        
        tree = tax.tree
        nodes = tree.get_filtered_nodes(2)
        print(tree.get_node_path(1))
        print(tree.get_node_path(131567))
        print(tree.get_node_path(1224))
        print(tree.get_node_path(2))
        
        print('filtered nodes#', len(nodes))

        tax = ncbi_taxonomy(path, filtered_nodes=nodes)
        print(tax.nodes.head())
        tree = tax.tree
        print(tree.valid())
        print (tree.num_sub_nodes(1))
        print(tree.get_node_path(863522))
        print (tree.get_children(1))
        tax.save("/tmp/test_Bacteria.json.gz")
        if 1:
            print(tax.get_node_path(863522))
            print(tax.get_name_path(863522))


    def test_viruses():    
        tax = ncbi_taxonomy(path)
        
        tree = tax.tree
        nodes = tree.get_filtered_nodes(10239)
        print(tree.get_node_path(1))
        print(tree.get_node_path(10239))
        
        print('filtered nodes#', len(nodes))

        tax = ncbi_taxonomy(path, filtered_nodes=nodes)
        print(tax.nodes.head())
        tree = tax.tree
        print(tree.valid())
        print (tree.num_sub_nodes(1))
        print(tree.get_node_path(727977))
        print (tree.get_children(1))
        tax.save("/tmp/test_viruses.json.gz")
        if 1:
            print(tax.get_node_path(727977))
            print(tax.get_name_path(727977))




    if 0:
        print("\n test default")
        test_default()
    if 0:
        print("\n test_Bacteria")        
        test_Bacteria()
    if 1:
        print("\n test_viruses")        
        test_viruses()        
    
    
