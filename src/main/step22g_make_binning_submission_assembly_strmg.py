import config
from task import Task
import os
import pandas as pd 
import shutil
from strmghelper import StrmgBinning


def make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger ):               
        
    if True:
        
        if os.path.exists(outputFile) or os.path.exists(outputFile + '.gz'):
            print("Output exists, skip " + outputFile)
            return 
        
        if not os.path.exists(os.path.join(rankPredFile)):
            raise Exception (rankPredFile + " does not found")
        
        pred = pd.read_csv(rankPredFile, index_col=0)
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))

        binner = StrmgBinning(has_taxid=False, has_binid=True)
        binner.set_comment("LSHVec binner based on rank " + use_rank)
        binner.set_sample_id(sample_id)

        for u, v in pred[use_rank].reset_index().values:
            if 1:
                if v > 0:
                    binner.add(u, taxid=None, binid=v)
                else:
                    binner.add(u, taxid=None, binid='2.1')
        logger.info("writing " + outputFile + ".tmp")
        binner.write(outputFile + ".tmp")
        shutil.move(outputFile + ".tmp", outputFile)
        
 
def make_binning_submission_cami2_assembly_strmg_gsa(filename):               

    def f(logger):
        RANKS = "species,strain".split(",")
        prefix = filename.replace(".fasta.gz", '_full_strmg') 

        for use_rank in RANKS:
            sample_id = "{}".format(prefix)
            outputFile = os.path.join(config.SUB_PATH, "{}.{}_binner.txt".format(prefix, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, '{}.pred.rank.merged.gz'.format(prefix))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_assembly_strmg_gsa-{}'.format(filename), lambda u: f(u))
    task() 
            
 
if __name__ == '__main__':
    pass
