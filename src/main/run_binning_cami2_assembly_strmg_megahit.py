#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''
from run_binning_cami2_assembly_marmg_gold_standard import run

if __name__ == '__main__':
    filename = 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz'    
    run(filename)
