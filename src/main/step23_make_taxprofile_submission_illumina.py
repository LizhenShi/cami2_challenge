import config
from task import Task
import os
import pandas as pd 
from tqdm import tqdm as tqdm
import shutil
from step23_make_taxprofile_submission_pacbio import make_tax_profile_submission
 

def make_taxprof_submission_cami2_toy_airways_illumina(sampleno, threshold):               

    def f(logger):
        if 1:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI_Airways_sample_{}_illumina.profile.th{}.txt".format(sampleno, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'CAMI_Airways_illumina_sample_{}.pred.rank.gz'.format(sampleno))
            seqlenfile = None
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=True)
                
    task = Task('make_taxprof_submission_cami2_toy_airways_illumina-{}-th{}'.format(sampleno, threshold), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_toy_mousegut_illumina(sampleno, threshold):               

    def f(logger):
        if 1:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "MOUSEGUT_sample_{}_illumina.profile.th{}.txt".format(sampleno, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'MOUSEGUT_illumina_sample_{}.pred.rank.gz'.format(sampleno))
            seqlenfile = None
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=True)
                
    task = Task('make_taxprof_submission_cami2_toy_mousegut_illumina-{}-th{}'.format(sampleno, threshold), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_patmg_illumina(threshold):               

    def f(logger):
        if 1:
            sample_id = "{}".format("partmg_illumina")
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_patmg__illumina.profile.th{}.txt".format(threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'patmg_CAMI2_short_read_merge.pred.rank.gz')
            seqlenfile = None
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=True)
                
    task = Task('make_taxprof_submission_cami2_patmg_illumina-{}-th{}'.format(0, threshold), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_marmg_illumina(sampleno, threshold):               

    def f(logger):
        if 1:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_marmg_illumina_sample_{}.profile.th{}.txt".format(sampleno, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'marmgCAMI2_short_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            seqlenfile = None
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=True)
                
    task = Task('make_taxprof_submission_cami2_marmg_illumina-{}-th{}'.format(sampleno, threshold), lambda u: f(u))
    task() 
    
    
def make_taxprof_submission_cami2_strmg_illumina(sampleno, threshold):               

    def f(logger):
        if 1:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_strmg_illumina_sample_{}.profile.th{}.txt".format(sampleno, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'strmgCAMI2_short_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            seqlenfile = None
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=True)
                
    task = Task('make_taxprof_submission_cami2_strmg_illumina-{}-th{}'.format(sampleno, threshold), lambda u: f(u))
    task()         

        
def run():
    isDebug = config.is_debug()
    if isDebug:
        for threshold in [0.1, 0.3, 0.7, 0, 1, 3, 5, 10, 20, 30]:
            for no in [4, 7, 10]:
                make_taxprof_submission_cami2_toy_airways_illumina(no, threshold)
    
            for no in [14, 15, 30][:2]:
                make_taxprof_submission_cami2_toy_mousegut_illumina(no, threshold)
    else:
        for threshold in [ 0.3, 0.7, 1, 3, 5, 10]:
            make_taxprof_submission_cami2_patmg_illumina(threshold)
            for no in range(10):
                make_taxprof_submission_cami2_marmg_illumina(no, threshold)
            for no in range(100):
                make_taxprof_submission_cami2_strmg_illumina(no, threshold)
             
        
if __name__ == '__main__':
    run()

