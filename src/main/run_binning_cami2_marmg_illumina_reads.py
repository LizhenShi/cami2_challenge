#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
from step1_fastq_to_seq_marmg import cami2_marmg_short_to_seq
from step2_predict_short_marmg import predict_cami2_marmg_short
from step22_make_binning_submission_illumina import make_binning_submission_cami2_marmg_illumina
 

def run(sample_no):
    cami2_marmg_short_to_seq(sample_no)
    predict_cami2_marmg_short(sample_no)
    make_binning_submission_cami2_marmg_illumina(sample_no)

    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))


def main():
    for sample_no in range(10):
        run(sample_no)

            
if __name__ == '__main__':
    main()
