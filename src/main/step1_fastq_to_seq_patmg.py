import config
import utils
from task import Task
import os
import pandas as pd 
from utils import check_output_file_exists


def cami2_patmg_short_to_seq(fileno):               
        
    def f(logger):
        if check_output_file_exists(os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_R{}.seq'.format(fileno)), logger):
            return
        
        filepath = os.path.join(config.RAW_PATH, 'patmg_CAMI2_short_read_R{}.fastq.gz'.format(fileno))
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, 'patmg_CAMI2_short_read_R{}.seq.gz'.format(fileno))
        
        cmd = 'python {}/src/main/fastqToSeq.py -i {} -o {} --cami2'.format(config.HOME, filepath, tmpoutpath)
        status = utils.shell_run_and_wait(cmd)
        if status != 0:
            utils.remove_if_file_exit(tmpoutpath)
            msg = "Run command failed: " + cmd
            logger.error(msg)
            raise Exception(msg)
        
        outpath = os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_R{}.seq.gz'.format(fileno))
        utils.move(tmpoutpath, outpath)
        
    task = Task('cami2_patmg_short_to_seq-{}'.format(fileno), lambda u: f(u))
    task() 


def cami2_patmg_short_seq_merge():               
        
    def f(logger):
        if check_output_file_exists(os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_merge.seq'), logger):
            return
        
        lst = []
        for fileno in [1, 2]:
            filepath = os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_R{}.seq.gz'.format(fileno))
            df1 = pd.read_csv(filepath, header=None, index_col=0, sep="\t").reset_index(drop=True)
            df1.columns = ['rid'+str(fileno), 'seq'+str(fileno)]
            lst.append(df1)
        
        df = pd.concat(lst, axis=1)
        df['seqm'] = df[["seq1", 'seq2']].apply(lambda u: "NNNN".join(u.values), axis=1)
        df['readid'] = df .index.map(lambda u: "R_" + str(u))
        
        df = df[['readid', 'seqm']]
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, 'patmg_CAMI2_short_read_merge.seq.gz')
        
        df.to_csv(tmpoutpath, compression="gzip", header=None, sep="\t")
        
        outpath = os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_merge.seq.gz')
        utils.move(tmpoutpath, outpath)
        
    task = Task('cami2_patmg_short_seq_merge', lambda u: f(u))
    task() 
    
def cami2_patmg_short_seq_concat():               
        
    def f(logger):
        if check_output_file_exists(os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_concat.seq'), logger):
            return
        
        lst = []
        for fileno in [1, 2]:
            filepath = os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_R{}.seq.gz'.format(fileno))
            df1 = pd.read_csv(filepath, header=None, index_col=0, sep="\t").reset_index(drop=True)
            df1.columns = ['rid', 'seq']
            lst.append(df1)
        
        df = pd.concat(lst, axis=0).reset_index(drop=True)
        df.index=df.index+1 
        
        df = df[['rid', 'seq']]
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, 'patmg_CAMI2_short_read_concat.seq.gz')
        
        df.to_csv(tmpoutpath, compression="gzip", header=None, sep="\t")
        
        outpath = os.path.join(config.SEQ_PATH, 'patmg_CAMI2_short_read_concat.seq.gz')
        utils.move(tmpoutpath, outpath)
        
    task = Task('cami2_patmg_short_seq_concat', lambda u: f(u))
    task()     

    
def run():
    mad_nos = [1, 2]
    for i in mad_nos:
        cami2_patmg_short_to_seq(i)
    cami2_patmg_short_seq_merge()
    cami2_patmg_short_seq_concat()     

if __name__ == '__main__':
    run()

