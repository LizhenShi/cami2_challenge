import config
from task import Task
import os
import pandas as pd 
from step23_make_taxprofile_submission_pacbio import make_tax_profile_submission
 

def make_taxprof_submission_cami2_patmg_illumina_with_virus(threshold):               

    def f(logger):
        if 1:
            sample_id = "{}".format("partmg_illumina")
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_patmg__illumina.profile.w_virus.th{}.txt".format(threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'patmg_CAMI2_short_read_merge.allpred.rank.gz')
            seqlenfile = None
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=True)
                
    task = Task('make_taxprof_submission_cami2_patmg_illumina_with_virus-{}-th{}'.format(0, threshold), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_assembly_gsa_with_vrius(filename, baserank, threshold):               

    def f(logger):
        prefix = filename.replace(".fasta.gz", '') 
        if 1:
            sample_id = "{}".format(prefix)
            outputFile = os.path.join(config.SUB_PATH, "{}.profile.w_virus.{}.th{}.txt.txt".format(prefix, baserank, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, '{}_full.allpred.rank.merged.gz'.format(prefix))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "{}.length.gz".format(prefix))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False, baserank="strain")
                
    task = Task('make_taxprof_submission_cami2_assembly_gsa_with_vrius-{}-{}-{}'.format(filename, baserank, threshold), lambda u: f(u))
    task() 

        
def run():
    isDebug = config.is_debug()
    if isDebug:
        pass
    else:
        for threshold in [ 0.3, 0.7, 1, 3, 5, 10]:
            make_taxprof_submission_cami2_patmg_illumina_with_virus(threshold)
             
        files = ['patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']
        
        for filename in files:
            for threshold in [0.3, 0.7, 1, 3, 5, 10]:
                make_taxprof_submission_cami2_assembly_gsa_with_vrius(filename, "strain", threshold)

        
if __name__ == '__main__':
    run()

