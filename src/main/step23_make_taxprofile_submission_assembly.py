import config
from task import Task
import os
import pandas as pd 
from step23_make_taxprofile_submission_pacbio import make_tax_profile_submission
        

def make_taxprof_submission_cami2_toy_airways_gsa(sampleno, threshold, baserank):               

    def f(logger):
        if 1:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI_Airways_sample_{}_gsa.profile.{}.th{}.txt".format(sampleno, baserank, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'CAMI_Airways_sample_{}_gsa_full.pred.rank.merged.gz'.format(sampleno))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "CAMI_Airways_sample_{}_gsa.length.gz".format(sampleno))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False, baserank=baserank)
                
    task = Task('make_taxprof_submission_cami2_toy_airways_gsa-{}-th{}-{}'.format(sampleno, threshold, baserank), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_toy_mousegut_gsa(sampleno, threshold, baserank):               

    def f(logger):
        if 1:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "MOUSEGUT_sample_{}_gsa.profile.{}.th{}.txt".format(sampleno, baserank, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'CAMI_MOUSEGUT_sample_{}_gsa_full.pred.rank.merged.gz'.format(sampleno))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "MOUSEGUT_sample_{}_gsa.length.gz".format(sampleno))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False, baserank=baserank)
                
    task = Task('make_taxprof_submission_cami2_toy_mousegut_gsa-{}-th{}-{}'.format(sampleno, threshold, baserank), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_assembly_gsa(filename, baserank, threshold):               

    def f(logger):
        prefix = filename.replace(".fasta.gz", '') 
        if 1:
            sample_id = "{}".format(prefix)
            outputFile = os.path.join(config.SUB_PATH, "{}.profile.{}.th{}.txt.txt".format(prefix, baserank, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, '{}_full.pred.rank.merged.gz'.format(prefix))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "{}.length.gz".format(prefix))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False, baserank="strain")
                
    task = Task('make_taxprof_submission_cami2_assembly_gsa-{}-{}-{}'.format(filename, baserank, threshold), lambda u: f(u))
    task() 
        
def run():
    isDebug = config.is_debug()
    if isDebug:
        for threshold in [0.1, 0.3, 0.7, 0, 1, 3, 5, 10, 20, 30]:
            for no in [4, 7, 10]:
                make_taxprof_submission_cami2_toy_airways_gsa(no, threshold, 'strain')
                make_taxprof_submission_cami2_toy_airways_gsa(no, threshold, 'genus')
    
            for no in [14, 15, 30]:
                make_taxprof_submission_cami2_toy_mousegut_gsa(no, threshold, 'strain')
                make_taxprof_submission_cami2_toy_mousegut_gsa(no, threshold, 'genus')
    else:
        files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
                 'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz',
                  'patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']
        
        for filename in files:
            for threshold in [0, 0.1, 0.3, 0.7, 1, 3, 5, 10]:
                make_taxprof_submission_cami2_assembly_gsa(filename, "strain", threshold)
                         
        
if __name__ == '__main__':
    run()

