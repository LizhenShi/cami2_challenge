#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
from step1_fastq_to_seq_marmg import cami2_marmg_short_to_seq
from step2_predict_short_marmg import predict_cami2_marmg_short
from step23_make_taxprofile_submission_illumina import make_taxprof_submission_cami2_marmg_illumina
from step24a_merge_taxprofile_marmg_submission import step24a_make_multiple_sample_taxprofile_submission_illumina


def run(sample_no):
    cami2_marmg_short_to_seq(sample_no)
    predict_cami2_marmg_short(sample_no)
    
    for threshold in [5]:
        make_taxprof_submission_cami2_marmg_illumina(sample_no, threshold)

    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))


def main():
    for sample_no in range(10):
        run(sample_no)
    step24a_make_multiple_sample_taxprofile_submission_illumina()

            
if __name__ == '__main__':
    main()
