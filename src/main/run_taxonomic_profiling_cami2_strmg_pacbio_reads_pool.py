#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
import run_taxonomic_profiling_cami2_strmg_pacbio_reads
from step24b_merge_taxprofile_strmg_submission import step24b_merge_taxprofile_strmg_submission_pacbio
 

def main():
    run_taxonomic_profiling_cami2_strmg_pacbio_reads.main()
    step24b_merge_taxprofile_strmg_submission_pacbio(5)
            
if __name__ == '__main__':
    main()
