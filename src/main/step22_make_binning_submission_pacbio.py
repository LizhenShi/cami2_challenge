import config
import utils
from task import Task
import os
import pandas as pd 
from myzstd import ZstdFile
from joblib import Parallel, delayed
from tqdm import tqdm as tqdm
from utils import check_output_file_exists
from helper import BinningHelper, Binning
import shutil

from utils import get_input_file
from step22_make_binning_submission_assembly import make_binning_submission


def make_binning_submission_cami2_toy_airway_pacbio(sampleno):               

    def f(logger):
        RANKS= "phylum,class,order,family,genus,species".split(",")
        for use_rank in RANKS:
            sample_id = "CAMI2_Human_Microbiome_sample_{}_pacbio".format(sampleno)
            outputFile=os.path.join(config.SUB_PATH, "CAMI2_Human_Microbiome_sample_{}_pacbio.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile=os.path.join(config.PRED_PATH, 'CAMI_Airways_pacbio_sample_{}.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_toy_airway_pacbio-{}'.format(sampleno), lambda u: f(u))
    task() 

def make_binning_submission_cami2_toy__mousegut_pacbio(sampleno):               

    def f(logger):
        RANKS= "phylum,class,order,family,genus,species".split(",")
        for use_rank in RANKS:
            sample_id = "CAMI2_Mouse_Gut_sample_{}_pacbio".format(sampleno)
            outputFile=os.path.join(config.SUB_PATH, "CAMI2_Mouse_Gut_sample_{}_pacbio.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile=os.path.join(config.PRED_PATH, 'MOUSEGUT_pacbio_sample_{}.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_toy__mousegut_pacbio-{}'.format(sampleno), lambda u: f(u))
    task() 

def make_binning_submission_cami2_marmg_pacbio(sampleno):               

    def f(logger):
        for use_rank in ['genus','species']:
            sample_id = "{}".format(sampleno)
            outputFile=os.path.join(config.SUB_PATH, "CAMI2_marmg_sample_{}_pacbio.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'marmgCAMI2_long_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_marmg_pacbio-{}'.format(sampleno), lambda u: f(u))
    task() 

def make_binning_submission_cami2_strmg_pacbio(sampleno):               

    def f(logger):
        for use_rank in ['genus','species']:
            sample_id = "{}".format(sampleno)
            outputFile=os.path.join(config.SUB_PATH, "CAMI2_strmg_sample_{}_pacbio.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'strmgCAMI2_long_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_strmg_pacbio-{}'.format(sampleno), lambda u: f(u))
    task() 
        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [4,7,10]:
            make_binning_submission_cami2_toy_airway_pacbio(no)

        for no in [14,15,30]:
            make_binning_submission_cami2_toy__mousegut_pacbio(no)
    else:
        for no in range(10):
            make_binning_submission_cami2_marmg_pacbio(no)
        for no in range(100):
            make_binning_submission_cami2_strmg_pacbio(no)
                         
        
if __name__ == '__main__':
    run()

