#!/usr/bin/env python

import config
from task import Task
import os
import pandas as pd 
from joblib import Parallel, delayed
from tqdm import tqdm as tqdm
from helper import BinningHelper, TaxonomicProfiling, TaxonomicHelper
import shutil


def make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold, logger, seqlenfile=None, is_illumina=False, baserank='strain'):               

    def groupby_df_by_rank(df, rank):
        df = df.copy()
        df[rank] = df['ranks'].map(lambda u: u[rank])
        df = df.groupby(rank)[['count']].sum()
        df['PERCENTAGE'] = df['count'] / df['count'].sum() * 100
        return df.sort_values('count', ascending=False)

    def make_profile(df, rank):
        df = groupby_df_by_rank(df, rank)
        df['TAXID'] = df.index.map(lambda u: binhelper.get_tax_bin_id(u, rank))
        df['RANK'] = rank
        df['abc'] = df['TAXID'].map(lambda u: helper.get_taxpath(u))
        df['RANK'] = df['abc'].map(lambda u: u[0])
        df['TAXPATH'] = df['abc'].map(lambda u: u[1])
        df['TAXPATHSN'] = df['abc'].map(lambda u: u[2])
        return df['TAXID    RANK    TAXPATH    TAXPATHSN    PERCENTAGE'.split()]
        
    if True:
        if os.path.exists(outputFile):
            print ("output existing, skip: " + outputFile)
            return 
        
        if not os.path.exists(os.path.join(rankPredFile)):
            raise Exception (rankPredFile + " does not found")

        pred = pd.read_csv(rankPredFile, index_col=0)
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))
        logger.info("pred['superkingdom'].value_counts():\n{}".format(pred['superkingdom'].value_counts()))

        if is_illumina: 
            pred['seqlen'] = 150
        else:
            readlen = pd.read_csv(seqlenfile , index_col=0)
            pred['seqlen'] = readlen['seqlen']

        logger.info("rank==-1:\n{}".format((pred == -1).mean()))
        
        for u in pred.columns:
            if u != 'seqlen':
                print ('-1 percentage:', u, pred.loc[ pred[u] == -1, 'seqlen'].sum() / pred['seqlen'].sum())

        RANKS = ['superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'strain']

        pred = pred [pred ['superkingdom'] == 2]
        
        profiler = TaxonomicProfiling()
        helper = TaxonomicHelper()
        binhelper = BinningHelper()
        
        profiler.set_comment("tax profile based on rank prediction")
        profiler.set_sample_id(sample_id)
        
        for rank in [baserank]:
            assert rank in ['strain', 'genus']
            df = pred[[rank, 'seqlen']]
            df = df[df[rank] > 0] 
            df = df.groupby(rank).sum().reset_index()
            df.index = df[rank].values
            df['taxlen'] = df.index.map(lambda u: helper.get_length(u))
            df['count'] = df['seqlen'] / df['taxlen']
            if rank == 'strain':
                df['ranks'] = df.index.map(lambda u:binhelper.get_ranks(u))
            else:
                df['ranks'] = df.index.map(lambda u:binhelper.get_genus_ranks(u))
            if threshold and threshold > 0:
                maxcount = df['count'].max()
                df = df[df['count'] >= maxcount * threshold]
            df['PERCENTAGE'] = df['count'] / df['count'].sum() * 100
            df = df.sort_values('count', ascending=False)
            straindf = df

        retlist = []
        for u in RANKS:
            if rank == 'strain' and u != 'strain':
                retlist.append(make_profile(straindf, u))
            elif rank == 'genus' and u not in ['species', 'strain']:
                retlist.append(make_profile(straindf, u))

        retdf = pd.concat(retlist)
        for row in retdf.values:
            profiler.add(*row)
            
        logger.info("writing " + outputFile + ".tmp")
        profiler.write(outputFile + ".tmp")
        shutil.move(outputFile + ".tmp", outputFile)
        

def make_taxprof_submission_cami2_toy_airways_pacbio(sampleno, threshold, baserank='strain'):               

    def f(logger):
        if 1:
            sample_id = "{}".format(sampleno)
            if baserank == 'strain':
                outputFile = os.path.join(config.SUB_PATH, "CAMI_Airways_sample_{}_pacbio.profile.th{}.txt".format(sampleno, threshold))
            else:
                outputFile = os.path.join(config.SUB_PATH, "CAMI_Airways_sample_{}_pacbio.profile.{}.th{}.txt".format(sampleno, baserank, threshold))
            rankPredFile = os.path.join(config.PRED_PATH, 'CAMI_Airways_pacbio_sample_{}.pred.rank.gz'.format(sampleno))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "CAMI_Airways_pacbio_sample_{}.length.gz".format(sampleno))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False)
                
    task = Task('make_taxprof_submission_cami2_toy_airways_pacbio-{}-th{}-{}'.format(sampleno, threshold, baserank), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_toy_mousegut_pacbio(sampleno, threshold, baserank='strain'):               

    def f(logger):
        if 1:
            if baserank == 'strain':
                outputFile = os.path.join(config.SUB_PATH, "MOUSEGUT_sample_{}_pacbio.profile.th{}.txt".format(sampleno, threshold))
            else:
                outputFile = os.path.join(config.SUB_PATH, "MOUSEGUT_sample_{}_pacbio.profile.{}.th{}.txt".format(sampleno, baserank, threshold))

            sample_id = "{}".format(sampleno)
            rankPredFile = os.path.join(config.PRED_PATH, 'MOUSEGUT_pacbio_sample_{}.pred.rank.gz'.format(sampleno))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "MOUSEGUT_pacbio_sample_{}.length.gz".format(sampleno))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False)
                
    task = Task('make_taxprof_submission_cami2_toy_mousegut_pacbio-{}-th{}-{}'.format(sampleno, threshold, baserank), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_marmg_pacbio(sampleno, threshold, baserank='strain'):               

    def f(logger):
        if 1:
            if baserank == 'strain':
                outputFile = os.path.join(config.SUB_PATH, "marmgCAMI2_sample_{}_pacbio.profile.th{}.txt".format(sampleno, threshold))
            else:
                outputFile = os.path.join(config.SUB_PATH, "marmgCAMI2_sample_{}_pacbio.profile.{}.th{}.txt".format(sampleno, baserank, threshold))

            sample_id = "{}".format(sampleno)
            rankPredFile = os.path.join(config.PRED_PATH, 'marmgCAMI2_long_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "marmgCAMI2_long_read_sample_{}_reads.length.gz".format(sampleno))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False)
                
    task = Task('make_taxprof_submission_cami2_marmg_pacbio-{}-th{}-{}'.format(sampleno, threshold, baserank), lambda u: f(u))
    task() 


def make_taxprof_submission_cami2_strmg_pacbio(sampleno, threshold, baserank='strain'):               

    def f(logger):
        if 1:
            if baserank == 'strain':
                outputFile = os.path.join(config.SUB_PATH, "strmgCAMI2_sample_{}_pacbio.profile.th{}.txt".format(sampleno, threshold))
            else:
                outputFile = os.path.join(config.SUB_PATH, "strmgCAMI2_sample_{}_pacbio.profile.{}.th{}.txt".format(sampleno, baserank, threshold))

            sample_id = "{}".format(sampleno)
            rankPredFile = os.path.join(config.PRED_PATH, 'strmgCAMI2_long_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            seqlenfile = os.path.join(config.SEQLEN_PATH, "strmgCAMI2_long_read_sample_{}_reads.length.gz".format(sampleno))
            make_tax_profile_submission(sample_id, rankPredFile, outputFile, threshold / 100.0, logger, seqlenfile, is_illumina=False)
                
    task = Task('make_taxprof_submission_cami2_strmg_pacbio-{}-th{}-{}'.format(sampleno, threshold, baserank), lambda u: f(u))
    task() 

                
def run():
    isDebug = config.is_debug()
    if isDebug:
        for threshold in [0.1, 0.3, 0.7, 0, 1, 3, 5, 10, 20, 30]:
            for no in [4, 7, 10]:
                make_taxprof_submission_cami2_toy_airways_pacbio(no, threshold)
    
            for no in [14, 15, 30]:
                make_taxprof_submission_cami2_toy_mousegut_pacbio(no, threshold)
    else:
        for threshold in [ 0.3, 0.7, 1, 3, 5, 10]:
            for no in range(10):
                make_taxprof_submission_cami2_marmg_pacbio(no, threshold)
            for no in range(100):
                make_taxprof_submission_cami2_strmg_pacbio(no, threshold)
                         
        
if __name__ == '__main__':
    run()

