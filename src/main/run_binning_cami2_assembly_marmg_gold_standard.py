#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''
from step11f_assembly_to_seq_full import cami2_gsa_to_seq_full
from step12c_make_assembly_seq_length import make_assembly_seq_length
from step12f_assembly_seq_predict_full import predict_cami2_gsa_full_task
from step20_make_rank_pred_assembly import make_rank_pred_cami2_4_assembly
from step21f_assembly_seq_rank_predict_merge import predict_cami2_gsa_full_merge_task
import config
from step22_make_binning_submission_assembly import make_binning_submission_cami2_assembly_gsa


def run(filename):

    cami2_gsa_to_seq_full(filename)

    make_assembly_seq_length(filename.replace('.fasta.gz', ""))
    
    prefix = filename.replace(".fasta.gz", '_full') 
    predict_cami2_gsa_full_task(prefix)
    
    make_rank_pred_cami2_4_assembly(filename)
    
    predict_cami2_gsa_full_merge_task(filename)
    
    make_binning_submission_cami2_assembly_gsa(filename)

    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))

        
if __name__ == '__main__':
    filename = 'marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz'    
    run(filename)
