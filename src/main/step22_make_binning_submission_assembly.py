import config
from task import Task
import os
import pandas as pd 
from helper import Binning
import shutil


def make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger, is_illumina=False):               
        
    if True:
        
        if os.path.exists(outputFile) or os.path.exists(outputFile + '.gz'):
            print("Output exists, skip " + outputFile)
            return 
        
        if not os.path.exists(os.path.join(rankPredFile)):
            raise Exception (rankPredFile + " does not found")
        
        pred = pd.read_csv(rankPredFile, index_col=0)
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))

        binner = Binning(has_taxid=True, has_binid=False)
        binner.set_comment("LSHVec binner based on rank " + use_rank)
        binner.set_sample_id(sample_id)

        for u, v in pred[use_rank].reset_index().values:
            if is_illumina:
                if v > 0:
                    binner.add(u + "/1", taxid=v)
                    binner.add(u + "/2", taxid=v)
                else:
                    binner.add(u + "/1", taxid=2)
                    binner.add(u + "/2", taxid=2)
            else:
                if v > 0:
                    binner.add(u, taxid=v)
                else:
                    binner.add(u, taxid=2)
        logger.info("writing " + outputFile + ".tmp")
        binner.write(outputFile + ".tmp")
        shutil.move(outputFile + ".tmp", outputFile)
        

def make_binning_submission_cami2_toy_airway_gsa(sampleno):               

    def f(logger):
        RANKS = "class,order,family,genus,species,strain".split(",")
        for use_rank in RANKS:
            sample_id = "CAMI2_Human_Microbiome_sample_{}_gsa".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_Human_Microbiome_sample_{}_gsa.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'CAMI_Airways_sample_{}_gsa_full.pred.rank.merged.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_toy_airway_gsa-{}'.format(sampleno), lambda u: f(u))
    task() 


def make_binning_submission_cami2_toy__mousegut_gsa(sampleno):               

    def f(logger):
        RANKS = "species,genus".split(",")
        for use_rank in RANKS:
            sample_id = "CAMI2_Mouse_Gut_sample_{}_gsa".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_Mouse_Gut_sample_{}_gsa.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'CAMI_MOUSEGUT_sample_{}_gsa_full.pred.rank.merged.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_toy__mousegut_gsa-{}'.format(sampleno), lambda u: f(u))
    task() 


def make_binning_submission_cami2_assembly_gsa(filename):               

    def f(logger):
        RANKS = "species,genus".split(",")
        prefix = filename.replace(".fasta.gz", '') 

        for use_rank in RANKS:
            sample_id = "{}".format(prefix)
            outputFile = os.path.join(config.SUB_PATH, "{}.{}_binner.txt".format(prefix, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, '{}_full.pred.rank.merged.gz'.format(prefix))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger)
                
    task = Task('make_binning_submission_cami2_assembly_gsa-{}'.format(filename), lambda u: f(u))
    task() 

            
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [4, 7, 10]:
            make_binning_submission_cami2_toy_airway_gsa(no)

        for no in [14, 15, 30]:
            make_binning_submission_cami2_toy__mousegut_gsa(no)
    else:
        files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
                 'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz',
                  'patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']        
        for filename in files:
            make_binning_submission_cami2_assembly_gsa(filename)

        
if __name__ == '__main__':
    run()

