#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
from step1_fastq_to_seq_strmg import cami2_strmg_short_to_seq
from step2_predict_short_strmg import predict_cami2_strmg_short
from step23_make_taxprofile_submission_illumina import make_taxprof_submission_cami2_strmg_illumina
 

def run(sample_no):
    cami2_strmg_short_to_seq(sample_no)
    predict_cami2_strmg_short(sample_no)
    
    for threshold in [5]:
        make_taxprof_submission_cami2_strmg_illumina(sample_no, threshold)

    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))

        
def main():
    for sample_no in range(100):
        run(sample_no)
            
            
if __name__ == '__main__':
    main()
