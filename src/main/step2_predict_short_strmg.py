import config
import utils
from task import Task
import os

from step2_predict_short_marmg import predict_cami2_illumina, \
    make_rank_predict_with_batch
from utils import check_output_file_exists
import gc
import sys

        
def predict_cami2_strmg_short(fileno):               
        
    def f(logger):
        prefix = 'strmgCAMI2_short_read_sample_{}_reads'.format(fileno)
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return
        predict_cami2_illumina(prefix, logger)
        make_rank_predict_with_batch(prefix, logger, only_bacteria=True)                
        gc.collect()
        
    task = Task('predict_cami2_strmg_short-{}'.format(fileno), lambda u: f(u))
    task() 


def run():
    isDebug = config.is_debug()
    
    mad_nos = [0] if isDebug else range(100)
        
    for i in mad_nos:
        predict_cami2_strmg_short(i)
        
           
if __name__ == '__main__':
    if len(sys.argv) == 1:
        run()
    elif len(sys.argv) == 3:
        if sys.argv[1] == 'strmg':
            fileno = int(sys.argv[2])
            predict_cami2_strmg_short(fileno)
    else:
        raise Exception("parse arg failed")
