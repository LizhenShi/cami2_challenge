import config
from task import Task
import os
import pandas as pd 
import numpy as np 

from step12f_assembly_seq_predict_full import SEQ_GSA_LENGTHS
import shutil


def assembly_seq_full_predict_merge(prefix, seqlenfile, logger):               
        
    if True:
        outpath = os.path.join(config.PRED_PATH, "{}.pred.rank.merged.gz".format(prefix))
        if os.path.exists(outpath):
            logger.info("skip " + outpath)
            return

        files = [os.path.join(config.PRED_PATH, '{}_l{}.pred.rank.gz'.format(prefix, length)) for length in SEQ_GSA_LENGTHS ]
        for filename in files:
            assert os.path.exists(filename), filename
        
        seqlen = pd.read_csv(seqlenfile, index_col=0)
        
        retlist = []
        for length in SEQ_GSA_LENGTHS:
            filepath = os.path.join(config.PRED_PATH, '{}_l{}.pred.rank.gz'.format(prefix, length))
            pred = pd.read_csv(filepath, index_col=0)
            if length == 150:
                a, b = 0, 400
            elif length == 500:
                a, b = 400, 1100
            elif length == 1000:
                a, b = 1100, 3500
            elif length == 3000:
                a, b = 3500, np.inf
            index = seqlen[(seqlen['seqlen'] >= a) & (seqlen['seqlen'] < b)].index
            retlist.append(pred.loc[index])
        
        df = pd.concat(retlist).sort_index()
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, "{}.pred.rank.merged.gz".format(prefix))
        logger.info("writing " + tmpoutpath)
        df.to_csv(tmpoutpath, compression='gzip')
        shutil.move(tmpoutpath, outpath)


def predict_cami2_toy_airway_gsa_full_merge(fileno):               
        
    def f(logger):
        prefix = 'CAMI_Airways_sample_{}_gsa_full'.format(fileno)
        seqlenfile = os.path.join(config.SEQLEN_PATH, "CAMI_Airways_sample_{}_gsa.length.gz".format(fileno))
        assembly_seq_full_predict_merge(prefix, seqlenfile, logger)
                
    task = Task('predict_cami2_toy_airway_gsa_full_merge-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_toy_mousegut_gsa_full_merge(fileno):               
        
    def f(logger):
        prefix = 'CAMI_MOUSEGUT_sample_{}_gsa_full'.format(fileno)
        seqlenfile = os.path.join(config.SEQLEN_PATH, "MOUSEGUT_sample_{}_gsa.length.gz".format(fileno))
        assembly_seq_full_predict_merge(prefix, seqlenfile, logger)

    task = Task('predict_cami2_toy_mousegut_gsa_full_merge-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_gsa_full_merge_task(filename):               
        
    def f(logger):
        prefix = filename.replace(".fasta.gz", '') 
        seqlenfile = os.path.join(config.SEQLEN_PATH, "{}.length.gz".format(prefix))
        assembly_seq_full_predict_merge(prefix + "_full", seqlenfile, logger)

    task = Task('predict_cami2_gsa_full_merge_task-{}'.format(filename), lambda u: f(u))
    task() 

        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        toy_airway_nos = [4, 7, 10, 8, 9, 10, 11, 12, 23, 26, 27][:3]
        for no in toy_airway_nos:
            predict_cami2_toy_airway_gsa_full_merge(no)

        toy_mousegut_nos = [14, 15, 30, 31, 3, 46, 52, 53, 60, 8][:3]
        for no in toy_mousegut_nos:
            predict_cami2_toy_mousegut_gsa_full_merge(no)
    else:
        files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
                 'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz',
                  'patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']
        for filename in files:
            predict_cami2_gsa_full_merge_task(filename)

        
if __name__ == '__main__':
    run()

