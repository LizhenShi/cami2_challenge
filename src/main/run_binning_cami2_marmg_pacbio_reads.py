#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
from step1_fastq_to_seq_marmg import     cami2_marmg_long_to_seq
from step2_predict_long_marmg import make_seq_length_marmg_long, \
    predict_cami2_marmg_long
from step20_make_rank_pred_pacbio import make_rank_pred_cami2_marmg_pacbio
from step22_make_binning_submission_pacbio import make_binning_submission_cami2_marmg_pacbio


def run(sample_no):
    cami2_marmg_long_to_seq(sample_no)
    make_seq_length_marmg_long(sample_no)
    predict_cami2_marmg_long(sample_no)
    
    make_rank_pred_cami2_marmg_pacbio(sample_no)
        
    make_binning_submission_cami2_marmg_pacbio(sample_no)

    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))


def main():
    for sample_no in range(10):
        run(sample_no)

            
if __name__ == '__main__':
    main()
