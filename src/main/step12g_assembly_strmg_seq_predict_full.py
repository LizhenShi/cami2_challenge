import config
import utils
from task import Task
import os

from utils import check_output_file_exists

SEQ_GSA_LENGTHS = [1000, 3000]

from utils import get_input_file


def get_gsa_models(length):
    modelname = 'model_199'
    inputModel = os.path.join(config.MODEL_PATH, 'strmg_refdb_model_gs_k23_l{}'.format(length), modelname)
    return inputModel


def predict_cami2_gsa_full(prefix, seqprefix, length, logger):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}_l{}.pred'.format(prefix, length)), logger):
            return
        
        inputModel = get_gsa_models(length)
        assert utils.file_exists(inputModel), inputModel

        hashfile = os.path.join(config.LSH_PATH, 'lsh_nt_NonEukaryota_k23_h25.crp')
        assert utils.file_exists(hashfile), hashfile

        jarpath = os.path.join(config.HOME, "lib", "jfastseq-*-*.jar")
        
        filepath = get_input_file(os.path.join(config.SEQ_PATH, '{}.seq'.format(seqprefix)), logger)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, '{}_l{}.pred.zst'.format(prefix, length))
        
        n_thread = utils.get_num_thread()
        cmd = "java -server -Xmx{} -XX:+AggressiveOpts -cp {} net.jfastseq.Main predict --hash {} --input {} --output {} --inputModel {} --max {} --threshold {} --withoutUncult --thread {} ".format(
        utils.get_java_memory(n_thread),
        jarpath,
        hashfile,
        filepath,
        tmpoutpath,
        inputModel,
        2000,
        0.001,
        n_thread)
        
        status = utils.shell_run_and_wait2(cmd)
        if status != 0:
            utils.remove_if_file_exit(tmpoutpath)
            msg = "Run command failed: " + cmd
            logger.error(msg)
            raise Exception(msg)
        
        outpath = os.path.join(config.PRED_PATH, '{}_l{}.pred.zst'.format(prefix, length))
        utils.move(tmpoutpath, outpath)


def predict_cami2_strmg_gsa_full_task(prefix,seqprefix):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            predict_cami2_gsa_full(prefix, seqprefix, length, logger)

    task = Task('predict_cami2_gsa_full_task-{}'.format(prefix), lambda u: f(u))
    task() 

        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        pass
    else:
        files = [ 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz']
        for filename in files:
            prefix = filename.replace(".fasta.gz", '_full_strmg') 
            predict_cami2_strmg_gsa_full_task(prefix)

        
if __name__ == '__main__':
    run()

