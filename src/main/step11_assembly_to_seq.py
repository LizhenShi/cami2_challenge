import config
import utils
from task import Task
import os
import sys

from Bio import SeqIO
import gzip
import numpy as np
import pandas as pd  
import shutil
from myzstd import ZstdFile
import re

sub_re = re.compile(r"[^ACGT]")

def read_fasta(fname):
    lst = []
    fasta_sequences = SeqIO.parse(gzip.open(fname, 'rt'), 'fasta')
    if 1:
        for fasta in fasta_sequences:
            name, sequence = fasta.id, str(fasta.seq)
            sequence = sub_re.sub("N", sequence.upper())
            lst.append([name, sequence])
    return lst


def gsa_fasta_to_seq(filepath, outpathprefix, logger):

    def get_intervals():
        a = [150, 300, 500, 1000, 3000, 6000]
        folders = [ 'wholeall_model_gs_k23_l{}'.format(u) for u in a]
        intervals = [0, 300, 500, 1000, 3000, 6000, sys.maxsize ]
        intervals = list(zip(intervals[:-1], intervals[1:], folders, a))
        return intervals    
                   
    def filter_seq(sequences, interval):
        lst = []
        a, b = interval[:2]
        for rid, seq in sequences:
            if len(seq) > a and len(seq) <= b:
                lst.append([rid, seq])
        return lst
    
    def filter_seq2(sequences, interval):
        lst = []
        a, b = interval[:2]
        for rid, seq in sequences:
            if len(seq) > a :
                lst.append([rid, seq])
        return lst    
        
    def make_subseq(sequences, interval, folder=10):

        def pad(seq, target_len):
            tmp = np.random.choice(['A', 'C', 'G', 'T'], target_len - len(seq)) 
            return seq + "".join(tmp)

        def g(seq, target_len):
            n1 = (len(seq) - target_len) / 15.0
            n2 = 1.0 * len(seq) / target_len * folder
            n = int(np.ceil(min(n1, n2)))
            n = max(1, n)
            if len(seq) < target_len:
                seq = pad(seq, target_len)
            
            ret = []
            for _ in range(n):
                i = int(np.random.random() * (len(seq) - target_len))
                ret.append(seq[i:i + target_len])
            return ret
    
        if False:
            filterd_sequences = filter_seq(sequences, interval)
        else:
            filterd_sequences = filter_seq2(sequences, interval)
        target_len = interval[-1]
        lst = []
        for rid, seq in filterd_sequences:
            for u in g(seq, target_len):
                lst.append([rid, u])
        return lst
        
    if True:
        logger.info("reading " + filepath)
        sequences = read_fasta(filepath)
        
        intervals = get_intervals()
        for interval in intervals:
            target_len = interval[-1]
            logger.info("processing for length " + str(target_len))            
            reads = make_subseq(sequences, interval, folder=10)
            outpath = '{}_l{}.seq.zst'.format(outpathprefix, target_len)
            if utils.file_exists(outpath): 
                logger.info("skip " + outpath)
                continue
            tmpoutpath = outpath + ".bk"
            logger.info("writing " + tmpoutpath)
            with ZstdFile(tmpoutpath, 'w') as fout:
                pd.DataFrame(reads).to_csv(fout, header=None, sep="\t")
            #pd.DataFrame(reads).to_csv(tmpoutpath, header=None, compression='gzip', sep="\t")
            shutil.move(tmpoutpath, outpath)


def toy_airway_gsa_to_seq(fileno):

    def f(logger):
        
        filepath = os.path.join(config.ASSEMBLY_PATH, 'CAMI_Airways_sample_{}_gsa.fasta.gz'.format(fileno))
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)
        
        outpathprefix = os.path.join(config.SEQ_PATH, 'CAMI_Airways_sample_{}_gsa'.format(fileno))
        gsa_fasta_to_seq(filepath, outpathprefix, logger)
        
    task = Task('toy_airway_gsa_to_seq-{}'.format(fileno), lambda u: f(u))
    task() 

def toy_mousegut_gsa_to_seq(fileno):

    def f(logger):

        filepath = os.path.join(config.ASSEMBLY_PATH, 'MOUSEGUT_sample_{}_gsa.fasta.gz'.format(fileno))
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)

        outpathprefix = os.path.join(config.SEQ_PATH, 'CAMI_MOUSEGUT_sample_{}_gsa'.format(fileno))
        gsa_fasta_to_seq(filepath, outpathprefix, logger)

    task = Task('toy_mousegut_gsa_to_seq-{}'.format(fileno), lambda u: f(u))
    task()


def cami2_gsa_to_seq(filename):

    def f(logger):
        
        filepath = os.path.join(config.ASSEMBLY_PATH, filename)
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)
        
        outpathprefix = os.path.join(config.SEQ_PATH, filename.replace('.fasta.gz', ''))
        gsa_fasta_to_seq(filepath, outpathprefix, logger)
        
    task = Task('cami2_gsa_to_seq-{}'.format(filename), lambda u: f(u))
    task()

        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [4,7,10]:
            toy_airway_gsa_to_seq(no)
        for no in [14,15,30]:
            toy_mousegut_gsa_to_seq(no)

    files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
             'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz']
    #Parallel(n_jobs=myutils.get_num_thread())(delayed(cami2_gsa_to_seq)(u) for u in (seqfiles))
    for filename in files:
        cami2_gsa_to_seq(filename)

        
if __name__ == '__main__':
    run()

