#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
import run_taxonomic_profiling_cami2_marmg_pacbio_reads
from step24a_merge_taxprofile_marmg_submission import step24a_merge_taxprofile_marmg_submission_pacbio
 

def main():
    for sample_no in range(10):
        run_taxonomic_profiling_cami2_marmg_pacbio_reads.run(sample_no)
    step24a_merge_taxprofile_marmg_submission_pacbio(5)

            
if __name__ == '__main__':
    main()
