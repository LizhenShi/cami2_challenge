import config
import utils
from task import Task
import os

from step2_predict_short_marmg import predict_cami2_illumina, \
    make_rank_predict_with_batch
from utils import check_output_file_exists
import gc
import sys

        
def predict_cami2_patmg_short():               
        
    def f(logger):
        prefix = 'patmg_CAMI2_short_read_merge'
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return
        predict_cami2_illumina(prefix, logger)
        make_rank_predict_with_batch(prefix, logger, only_bacteria=True)                
        gc.collect()
        
    task = Task('predict_cami2_patmg_short', lambda u: f(u))
    task() 


def run():
    predict_cami2_patmg_short()
        
           
if __name__ == '__main__':
    run()
