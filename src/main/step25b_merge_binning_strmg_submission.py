import config
from task import Task
import os
import pandas as pd 
import shutil
import numpy as np  
import gzip
from step25a_merge_binning_marmg_submission import make_multiple_sample_binning
 

def step25b_make_multiple_sample_binning_submission_illumina():               

    def f(logger):
        make_multiple_sample_binning('CAMI2_strmg', 'illumina', 10, logger)
                    
    task = Task('step25b_make_multiple_sample_binning_submission_illumina-th{}'.format(), lambda u: f(u))
    task() 

    
def step25b_make_multiple_sample_binning_submission_pacbio():               

    def f(logger):
        make_multiple_sample_binning('strmgCAMI2', 'pacbio', 10, logger)
                    
    task = Task('step25b_make_multiple_sample_binning_submission_pacbio-th{}'.format(), lambda u: f(u))
    task()        

        
def run():
    isDebug = config.is_debug()
    if isDebug:
        pass
    else:
        step25b_make_multiple_sample_binning_submission_illumina()
        step25b_make_multiple_sample_binning_submission_pacbio()        
             
        
if __name__ == '__main__':
    run()

