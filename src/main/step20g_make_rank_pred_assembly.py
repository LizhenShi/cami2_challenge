import config
import utils
from task import Task
import os
import pandas as pd 
from myzstd import ZstdFile
from joblib import Parallel, delayed
from tqdm import tqdm as tqdm
from utils import check_output_file_exists
import shutil

from utils import get_input_file
from step12g_assembly_strmg_seq_predict_full import SEQ_GSA_LENGTHS
from strmghelper import StrmgBinningHelper
 

 

def parse_prob_text(text):
    if isinstance(text, str):
        l = [u.split(':') for u in text.split()]
        a = [int(u[0]) for u in l]
        b = [float(u[1]) for u in l]
        d = {k:v for k, v in zip(a, b)  }
        return d
    return None


def read_pred(fname):
    lines = []
    with ZstdFile(fname, 'r') as fin:
        for line in fin:
            lines.append(line.split("\t"))

    pred = pd.DataFrame(lines)
    pred.columns = ['readid', 'text']
    pred['prob'] = Parallel(n_jobs=utils.get_num_thread())(delayed(parse_prob_text)(u) for u in tqdm(pred['text'].values))
    pred = pred.drop('text', axis=1)
    print (fname, pred.shape)
    return pred.set_index('readid')


def find_best_rank(u):
    global target_rank, binhelper
    maxp = -1
    maxr = -1
    for k, v in u.items():
        if binhelper.is_in_rank(k, target_rank):
            if maxp < v:
                maxp = v
                maxr = k
    return maxr


def make_rank_predict(prefix, logger):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred.rank'.format(prefix)), logger):
            return

        fname = get_input_file(os.path.join(config.PRED_PATH, '{}.pred'.format(prefix)), logger)
        logger.info("reading prediction from " + fname)
        pred = read_pred(fname)
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))

        global target_rank, binhelper
        binhelper = StrmgBinningHelper()
        RANKS = binhelper.ranks
        for target_rank in RANKS:
            logger.info("to find best rank pred for " + target_rank)
            pred[target_rank] = utils.parallell_for(pred['prob'].values, find_best_rank, n_jobs=utils.get_num_thread())
            pred[target_rank].value_counts().head(10)
        
        pred = pred[RANKS]
        logger.info("pred.shape={}\n{}".format(str(pred.shape), pred.head()))
        
        df = pred
        tmpoutpath = os.path.join(config.LOCAL_PATH, "{}.pred.rank.gz".format(prefix))
        logger.info("writing " + tmpoutpath)
        df.to_csv(tmpoutpath, compression='gzip')
        outpath = os.path.join(config.PRED_PATH, "{}.pred.rank.gz".format(prefix))
        shutil.move(tmpoutpath, outpath)
        
def make_rank_pred_cami2_4_assembly_strmg(filename):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = filename.replace(".fasta.gz", "")
            prefix = '{}_full_strmg_l{}'.format(prefix, length)            
            make_rank_predict(prefix, logger)

    task = Task('make_rank_pred_cami2_4_assembly_strmg-{}'.format(filename), lambda u: f(u))
    task() 

            
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        pass
    else:
        files = [ 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz']
        for filename in files:
            make_rank_pred_cami2_4_assembly_strmg(filename)

        
if __name__ == '__main__':
    run()

