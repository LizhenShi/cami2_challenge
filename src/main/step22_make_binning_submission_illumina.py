import config
from task import Task
import os

from step22_make_binning_submission_assembly import make_binning_submission


def make_binning_submission_cami2_toy_airway_illumina(sampleno):               

    def f(logger):
        # RANKS= "phylum,class,order,family,genus,species".split(",")
        RANKS = ["species"] 
        for use_rank in RANKS:
            sample_id = "CAMI2_Human_Microbiome_sample_{}_illumina".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_Human_Microbiome_sample_{}_illumina.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'CAMI_Airways_illumina_sample_{}.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger, is_illumina=True)
                
    task = Task('make_binning_submission_cami2_toy_airway_illumina-{}'.format(sampleno), lambda u: f(u))
    task() 


def make_binning_submission_cami2_toy__mousegut_illumina(sampleno):               

    def f(logger):
        # RANKS= "phylum,class,order,family,genus,species".split(",")
        RANKS = ["species"] 
        for use_rank in RANKS:
            sample_id = "CAMI2_Mouse_Gut_sample_{}_illumina".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "CAMI2_Mouse_Gut_sample_{}_illumina.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'MOUSEGUT_illumina_sample_{}.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger, is_illumina=True)
                
    task = Task('make_binning_submission_cami2_toy__mousegut_illumina-{}'.format(sampleno), lambda u: f(u))
    task() 


def make_binning_submission_cami2_marmg_illumina(sampleno):               

    def f(logger):
        # RANKS= "phylum,class,order,family,genus,species".split(",")
        RANKS = ["species"] 
        for use_rank in RANKS:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "marmgCAMI2_sample_{}_short.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'marmgCAMI2_short_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger, is_illumina=True)
                
    task = Task('make_binning_submission_cami2_marmg_illumina-{}'.format(sampleno), lambda u: f(u))
    task() 


def make_binning_submission_cami2_strmg_illumina(sampleno):               

    def f(logger):
        # RANKS= "phylum,class,order,family,genus,species".split(",")
        RANKS = ["species"] 
        for use_rank in RANKS:
            sample_id = "{}".format(sampleno)
            outputFile = os.path.join(config.SUB_PATH, "strmgCAMI2_sample_{}_short.{}_binner.txt".format(sampleno, use_rank))
            rankPredFile = os.path.join(config.PRED_PATH, 'strmgCAMI2_short_read_sample_{}_reads.pred.rank.gz'.format(sampleno))
            make_binning_submission(sample_id, rankPredFile, outputFile, use_rank, logger, is_illumina=True)
                
    task = Task('make_binning_submission_cami2_strmg_illumina-{}'.format(sampleno), lambda u: f(u))
    task() 

        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [4, 7, 10]:
            make_binning_submission_cami2_toy_airway_illumina(no)

        for no in [14, 15, 30][:2]:
            make_binning_submission_cami2_toy__mousegut_illumina(no)
    else:
        for i in  range(10):
            make_binning_submission_cami2_marmg_illumina(i) 
                    
        for i in  range(100):
            make_binning_submission_cami2_strmg_illumina(i)            

        
if __name__ == '__main__':
    run()

