#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import config
import run_taxonomic_profiling_cami2_assembly_marmg_gold_standard
from step20b_make_rank_pred_patmg_with_virus import make_cami2_patmg_assembly_rank_pred_with_virus, \
    predict_cami2_patmg_full_with_virus_merge_task, \
    make_cami2_patmg_short_rank_pred_with_virus
from step23_make_taxprofile_submission_4_patmg_with_virus import make_taxprof_submission_cami2_assembly_gsa_with_vrius, \
    make_taxprof_submission_cami2_patmg_illumina_with_virus
import step1_fastq_to_seq_patmg
from step2_predict_short_patmg import predict_cami2_patmg_short
from step23_make_taxprofile_submission_illumina import make_taxprof_submission_cami2_patmg_illumina
import os
import pandas as pd 
import numpy as np 
from helper import TaxonomicProfiling

 
def make_taxprof_patmg_illumina():
    
    step1_fastq_to_seq_patmg.run()
    predict_cami2_patmg_short()
    make_cami2_patmg_short_rank_pred_with_virus()
    
    for threshold in [5]:
        make_taxprof_submission_cami2_patmg_illumina(threshold)
        make_taxprof_submission_cami2_patmg_illumina_with_virus(threshold)
    

def make_taxprof_patmg_assembly_w_virus():
    files = ['patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz']
    for filename in files:
        make_cami2_patmg_assembly_rank_pred_with_virus(filename)
        predict_cami2_patmg_full_with_virus_merge_task(filename)
        for threshold in [5]:
            make_taxprof_submission_cami2_assembly_gsa_with_vrius(filename, "strain", threshold)

    
def make_taxprof_patmg_assembly():
    
    files = ['patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz']
    for filename in files:
        run_taxonomic_profiling_cami2_assembly_marmg_gold_standard.run(filename)


def find_max_probability_genus():
    tp = TaxonomicProfiling()

    def show_file(filename, rank, b_show=True):
        df = pd.DataFrame(tp.read(filename))
        df[4] = df[4].astype(np.float32)
        df = df[df[4] > 0]
        df = df[df[1] == rank].sort_values(4, ascending=False)
        df = df.set_index(0)
        return df
    
    def merge_concat(dflist):
        dflist = [u.reset_index() for u in dflist]
        df = pd.concat(dflist)
        df = df[df[0].map(lambda u: u[-2] != '.')]
        df = df[df[4] > 1]
        df = df.sort_values(4, ascending=False)
        return df
    
    files = ['CAMI2_patmg__illumina.profile.th5.txt',
 'CAMI2_patmg__illumina.profile.w_virus.th5.txt',
 'patmgCAMI2_spades_mc_assembly.profile.strain.th5.txt.txt',
 'patmgCAMI2_spades_mc_assembly.profile.w_virus.strain.th5.txt.txt',
 'patmgCAMI2_spades_meta_assembly.profile.strain.th5.txt.txt',
 'patmgCAMI2_spades_meta_assembly.profile.w_virus.strain.th5.txt.txt']

    files = [os.path.join(config.SUB_PATH, u) for u in files]
    
    for rank in ['genus']:
        dflist = []        
        for filename in files:
            a = show_file(filename, rank, b_show=False)
            a = a[a[4] > a[4].max() * 0.15]
            dflist.append(a)
    
    df = merge_concat(dflist)
    gdf = df.groupby(0)[4].sum().sort_values(ascending=False)
    genus_ids = gdf.index.to_list()
    print("Found candidate genus:")
    print("genus\tscore")
    for u in genus_ids:
        print("{}\t{}".format(u, gdf.loc[u]))
        
    with open(os.path.join(config.SUB_PATH, 'taxa.txt'),'wt') as fout:
        fout.write("\t".join(genus_ids))
    
    with open(os.path.join(config.SUB_PATH, 'pathogen.txt'),'wt') as fout:
        fout.write(genus_ids[0])
        

def main():
    make_taxprof_patmg_assembly()
    make_taxprof_patmg_assembly_w_virus()
    make_taxprof_patmg_illumina()
    
    find_max_probability_genus()
    
    print ("""
    
    Finish running. Please find the result in {}. 
    
    """.format(config.SUB_PATH))
                
            
if __name__ == '__main__':
    main()
