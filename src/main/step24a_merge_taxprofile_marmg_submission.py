import config
from task import Task
import os
import pandas as pd 
import shutil
from helper import TaxonomicProfiling
import numpy as np  


def merge_samples_taxprof(prefix, readtype, threshold, n_samples, logger):

    def get_file(sample_no):
            if readtype == 'illumina':
                filename = "{}_{}_sample_{}.profile.th{}.txt".format(prefix, readtype, sample_no, threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                if os.path.exists(filename):
                    return filename 
                filename = "{}_{}_sample_{}.profile.{}.th{}.txt".format(prefix, readtype, sample_no, 'strain', threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                return filename
            elif readtype == 'pacbio':
                filename = "{}_sample_{}_{}.profile.th{}.txt".format(prefix, sample_no, readtype, threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                if os.path.exists(filename):
                    return filename 
                filename = "{}_sample_{}_{}.profile.{}.th{}.txt".format(prefix, sample_no, readtype, 'strain', threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                return filename

    if True:
        samplenos = range(n_samples)

        outputFile = os.path.join(config.SUB_PATH, "{}_sample_{}_{}.profile.th{}.txt".format(prefix, 'aggregate', readtype, threshold))
        if os.path.exists(outputFile):
            print ("output existing, skip: " + outputFile)
            return 
        
        ranks = 'superkingdom|phylum|class|order|family|genus|species'.split('|')
        filenames = [get_file(sample_no)                      for sample_no in samplenos]
        
        tp = TaxonomicProfiling()
        lst = []
        for i, filename in enumerate(filenames):
            df = pd.DataFrame(tp.read(filename))
            lst.append(df)
        df = pd.concat(lst)
        df[4] = df[4].astype(np.float32)
        df = df[df[4] > 0]
        df2 = df.groupby([0, 1, 2, 3]).sum().reset_index()
        df2[4] = df2[4] / len(samplenos)
        df2["rankid"] = df2[1].map(lambda u:-ranks.index(u))
        df2 = df2.sort_values(["rankid", 4], ascending=False)
        df2 = df2[df2[4] > 0.1].drop('rankid', axis=1)
        
        profiler = TaxonomicProfiling()

        for row in df2.values:
            profiler.add(*row)
            
        logger.info("writing " + outputFile + ".tmp")
        profiler.write(outputFile + ".tmp")
        shutil.move(outputFile + ".tmp", outputFile)


def make_multiple_sample_taxprof(prefix, readtype, threshold, n_samples, logger):

    def get_file(sample_no):
            if readtype == 'illumina':
                filename = "{}_{}_sample_{}.profile.th{}.txt".format(prefix, readtype, sample_no, threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                if os.path.exists(filename):
                    return filename 
                filename = "{}_{}_sample_{}.profile.{}.th{}.txt".format(prefix, readtype, sample_no, 'strain', threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                return filename
            elif readtype == 'pacbio':
                filename = "{}_sample_{}_{}.profile.th{}.txt".format(prefix, sample_no, readtype, threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                if os.path.exists(filename):
                    return filename 
                filename = "{}_sample_{}_{}.profile.{}.th{}.txt".format(prefix, sample_no, readtype, 'strain', threshold)
                filename = os.path.join(config.SUB_PATH, filename)
                return filename

    if True:
        samplenos = range(n_samples)

        outputFile = os.path.join(config.SUB_PATH, "{}_multiple_sample_{}.profile.th{}.txt".format(prefix, readtype, threshold))
        if os.path.exists(outputFile):
            print ("output existing, skip: " + outputFile)
            return 
        
        filenames = [get_file(sample_no)                      for sample_no in samplenos]
        
        with open(outputFile + ".tmp", 'wt') as fout:
            for i, filename in enumerate(filenames):
                with open(filename, 'rt') as fin:
                    for line in fin:
                        if line.startswith('@Version:'):
                            line = '@Version:0.10.0\n'
                        fout.write(line)
                fout.write("\n")
                
        shutil.move(outputFile + ".tmp", outputFile)    

    
def step24a_merge_taxprofile_marmg_submission_illumina(threshold=5):               

    def f(logger):
        merge_samples_taxprof('CAMI2_marmg', 'illumina', threshold, 10, logger)
                    
    task = Task('step24a_merge_taxprofile_marmg_submission_illumina-th{}'.format(threshold), lambda u: f(u))
    task() 

    
def step24a_merge_taxprofile_marmg_submission_pacbio(threshold=5):               

    def f(logger):
        merge_samples_taxprof('marmgCAMI2', 'pacbio', threshold, 10, logger)
                    
    task = Task('step24a_merge_taxprofile_marmg_submission_pacbio-th{}'.format(threshold), lambda u: f(u))
    task()  

    
def step24a_make_multiple_sample_taxprofile_submission_illumina(threshold=5):               

    def f(logger):
        make_multiple_sample_taxprof('CAMI2_marmg', 'illumina', threshold, 10, logger)
                    
    task = Task('step24a_make_multiple_sample_taxprofile_submission_illumina-th{}'.format(threshold), lambda u: f(u))
    task() 

    
def step24a_make_multiple_sample_taxprofile_submission_pacbio(threshold=5):               

    def f(logger):
        make_multiple_sample_taxprof('marmgCAMI2', 'pacbio', threshold, 10, logger)
                    
    task = Task('step24a_make_multiple_sample_taxprofile_submission_pacbio-th{}'.format(threshold), lambda u: f(u))
    task()        

        
def run():
    isDebug = config.is_debug()
    if isDebug:
        pass
    else:
        step24a_merge_taxprofile_marmg_submission_pacbio(5)
        step24a_merge_taxprofile_marmg_submission_illumina(5)
        step24a_make_multiple_sample_taxprofile_submission_illumina(5)
        step24a_make_multiple_sample_taxprofile_submission_pacbio(5)        
             
        
if __name__ == '__main__':
    run()

