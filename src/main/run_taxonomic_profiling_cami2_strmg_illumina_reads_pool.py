#!/usr/bin/env python

'''
Created on Aug 15, 2019

@author: bo
'''

import run_taxonomic_profiling_cami2_strmg_illumina_reads
from step24b_merge_taxprofile_strmg_submission import step24b_merge_taxprofile_strmg_submission_illumina
 

def main():
    run_taxonomic_profiling_cami2_strmg_illumina_reads.main()
    step24b_merge_taxprofile_strmg_submission_illumina(5)
            
            
if __name__ == '__main__':
    main()
