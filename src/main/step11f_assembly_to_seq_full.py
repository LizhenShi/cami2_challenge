import config
import utils
from task import Task
import os

import pandas as pd  
import shutil
from myzstd import ZstdFile

from step11_assembly_to_seq import read_fasta 


def gsa_fasta_to_seq_full(filepath, outpathprefix, logger):

    outpath = '{}_{}.seq.zst'.format(outpathprefix, 'full')
    if utils.file_exists(outpath): 
        logger.info("skip " + outpath)
        return
 
    logger.info("reading " + filepath)
    sequences = read_fasta(filepath)
    
    if True:
        logger.info("processing " + filepath)            
        
        reads = []
        for rid, seq in sequences:
            reads.append([rid, seq])
        assert len(reads)>0
        tmpoutpath = outpath + ".bk"
        logger.info("writing " + tmpoutpath)
        with ZstdFile(tmpoutpath, 'w') as fout:
            pd.DataFrame(reads).to_csv(fout, header=None, sep="\t")
        shutil.move(tmpoutpath, outpath)


def toy_airway_gsa_to_seq_full(fileno):

    def f(logger):
        
        filepath = os.path.join(config.ASSEMBLY_PATH, 'CAMI_Airways_sample_{}_gsa.fasta.gz'.format(fileno))
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)
        
        outpathprefix = os.path.join(config.SEQ_PATH, 'CAMI_Airways_sample_{}_gsa'.format(fileno))
        gsa_fasta_to_seq_full(filepath, outpathprefix, logger)
        
    task = Task('toy_airway_gsa_to_seq_full-{}'.format(fileno), lambda u: f(u))
    task() 


def toy_mousegut_gsa_to_seq_full(fileno):

    def f(logger):

        filepath = os.path.join(config.ASSEMBLY_PATH, 'MOUSEGUT_sample_{}_gsa.fasta.gz'.format(fileno))
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)

        outpathprefix = os.path.join(config.SEQ_PATH, 'CAMI_MOUSEGUT_sample_{}_gsa'.format(fileno))
        gsa_fasta_to_seq_full(filepath, outpathprefix, logger)

    task = Task('toy_mousegut_gsa_to_seq_full-{}'.format(fileno), lambda u: f(u))
    task()


def cami2_gsa_to_seq_full(filename):

    def f(logger):
        
        filepath = os.path.join(config.ASSEMBLY_PATH, filename)
        if not utils.file_exists(filepath):
            msg = "Input file does not exists: " + filepath
            logger.error(msg)
            raise Exception(msg)
        
        outpathprefix = os.path.join(config.SEQ_PATH, filename.replace('.fasta.gz', ''))
        gsa_fasta_to_seq_full(filepath, outpathprefix, logger)
        
    task = Task('cami2_gsa_to_seq_full-{}'.format(filename), lambda u: f(u))
    task()

        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        for no in [4, 7, 10]:
            toy_airway_gsa_to_seq_full(no)
        for no in [14, 15, 30]:
            toy_mousegut_gsa_to_seq_full(no)

    files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
             'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz']
    # Parallel(n_jobs=myutils.get_num_thread())(delayed(cami2_gsa_to_seq)(u) for u in (seqfiles))
    for filename in files:
        cami2_gsa_to_seq_full(filename)
        
    if 1:
        filepath1= os.path.join(config.ASSEMBLY_PATH, 'spades_patmg_meta','contigs.fasta')
        filepath2= os.path.join(config.ASSEMBLY_PATH, 'patmgCAMI2_spades_meta_assembly.fasta.gz')
        if not utils.file_exists(filepath2):
            cmd ="cat {} | gzip > {}".format(filepath1, filepath2)
            status = utils.shell_run_and_wait2(cmd)
            assert status ==0, cmd
        cami2_gsa_to_seq_full(filepath2.split("/")[-1])

    if 1:
        filepath1= os.path.join(config.ASSEMBLY_PATH, 'spades_patmg_mc','contigs.fasta')
        filepath2= os.path.join(config.ASSEMBLY_PATH, 'patmgCAMI2_spades_mc_assembly.fasta.gz')
        if not utils.file_exists(filepath2):
            cmd ="cat {} | gzip > {}".format(filepath1, filepath2)
            status = utils.shell_run_and_wait2(cmd)
            assert status ==0, cmd
        cami2_gsa_to_seq_full(filepath2.split("/")[-1])

    if 1:
        filepath1= os.path.join(config.ASSEMBLY_PATH, 'patmg_magahit_asemble','assembly_results', 'final.contigs.fa')
        filepath2= os.path.join(config.ASSEMBLY_PATH, 'patmgCAMI2_magahit_assembly.fasta.gz')
        if not utils.file_exists(filepath2):
            cmd ="cat {} | gzip > {}".format(filepath1, filepath2)
            status = utils.shell_run_and_wait2(cmd)
            assert status ==0, cmd
        cami2_gsa_to_seq_full(filepath2.split("/")[-1])
                
if __name__ == '__main__':
    run()

