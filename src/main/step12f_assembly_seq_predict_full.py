import config
import utils
from task import Task
import os

from utils import check_output_file_exists

SEQ_GSA_LENGTHS = [150, 500, 1000, 3000]

from utils import get_input_file


def get_gsa_models(length):
    modelname = 'model_1419' if length in [60020] else 'model_299'
    inputModel = os.path.join(config.MODEL_PATH, 'genbank_model_gs_k23_l{}'.format(length), modelname)
    return inputModel


def predict_cami2_gsa_full(prefix, length, logger):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}_l{}.pred'.format(prefix, length)), logger):
            return
        
        inputModel = get_gsa_models(length)
        assert utils.file_exists(inputModel), inputModel

        hashfile = os.path.join(config.LSH_PATH, 'lsh_nt_NonEukaryota_k23_h25.crp')
        assert utils.file_exists(hashfile), hashfile

        jarpath = os.path.join(config.HOME, "lib", "jfastseq-*-*.jar")
        
        filepath = get_input_file(os.path.join(config.SEQ_PATH, '{}.seq'.format(prefix)), logger)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, '{}_l{}.pred.zst'.format(prefix, length))
        
        n_thread = utils.get_num_thread()
        cmd = "java -server -Xmx{} -XX:+AggressiveOpts -cp {} net.jfastseq.Main predict --hash {} --input {} --output {} --inputModel {} --max {} --threshold {} --withoutUncult --thread {} ".format(
        utils.get_java_memory(n_thread),
        jarpath,
        hashfile,
        filepath,
        tmpoutpath,
        inputModel,
        500,
        0.005,
        n_thread)
        
        status = utils.shell_run_and_wait2(cmd)
        if status != 0:
            utils.remove_if_file_exit(tmpoutpath)
            msg = "Run command failed: " + cmd
            logger.error(msg)
            raise Exception(msg)
        
        outpath = os.path.join(config.PRED_PATH, '{}_l{}.pred.zst'.format(prefix, length))
        utils.move(tmpoutpath, outpath)


def predict_cami2_toy_airway_gsa_full(fileno):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = 'CAMI_Airways_sample_{}_gsa_full'.format(fileno)
            predict_cami2_gsa_full(prefix, length, logger)
                
    task = Task('predict_cami2_toy_airway_gsa_full-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_toy_mousegut_gsa_full(fileno):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            prefix = 'CAMI_MOUSEGUT_sample_{}_gsa_full'.format(fileno)
            predict_cami2_gsa_full(prefix, length, logger)

    task = Task('predict_cami2_toy_mousegut_gsa_full-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_gsa_full_task(prefix):               
        
    def f(logger):
        for length in SEQ_GSA_LENGTHS:
            predict_cami2_gsa_full(prefix, length, logger)

    task = Task('predict_cami2_gsa_full_task-{}'.format(prefix), lambda u: f(u))
    task() 

        
def run():
    isDebug = config.is_debug()
    
    if isDebug:
        toy_airway_nos = [4, 7, 10, 8, 9, 10, 11, 12, 23, 26, 27][:3]
        for no in toy_airway_nos:
            predict_cami2_toy_airway_gsa_full(no)

        toy_mousegut_nos = [14, 15, 30, 31, 3, 46, 52, 53, 60, 8][:3]
        for no in toy_mousegut_nos:
            predict_cami2_toy_mousegut_gsa_full(no)
    else:
        files = ['marmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_gold_standard_assembly.fasta.gz',
                 'marmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz', 'strmgCAMI2_short_read_pooled_megahit_assembly.fasta.gz',
                  'patmgCAMI2_spades_meta_assembly.fasta.gz', 'patmgCAMI2_spades_mc_assembly.fasta.gz', 'patmgCAMI2_magahit_assembly.fasta.gz']
        for filename in files:
            prefix = filename.replace(".fasta.gz", '_full') 
            predict_cami2_gsa_full_task(prefix)

        
if __name__ == '__main__':
    run()

