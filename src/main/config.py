
import os
import utils
import platform

if 'CAMI2_CHALLENGE_HOME' not in os.environ:
    HOME = os.path.join(os.environ['HOME'], 'cami2_challenge')
else:
    HOME = os.environ['CAMI2_CHALLENGE_HOME']

assert HOME is not None 

HOSTNAME = platform.node()

IS_BRIDGES = ("bridges" in HOSTNAME or ("SLURM_CLUSTER_NAME" in os.environ and "bridges" in os.environ["SLURM_CLUSTER_NAME"])) and not HOSTNAME.startswith('br')
if IS_BRIDGES:
    assert ("SLURM_CLUSTER_NAME" in os.environ and "bridges" in os.environ["SLURM_CLUSTER_NAME"])

INPUT_PATH = os.path.join(HOME, "input")
TASK_PATH = os.path.join(INPUT_PATH, 'task')
INFO_PATH = os.path.join(INPUT_PATH, 'info')
SEQ_PATH = os.path.join(INPUT_PATH, 'seqs')
MODEL_PATH = os.path.join(INPUT_PATH, 'model')
RAW_PATH = os.path.join(INPUT_PATH, 'raw')
PRED_PATH = os.path.join(INPUT_PATH, 'pred')
LSH_PATH = os.path.join(INPUT_PATH, 'lsh')
ASSEMBLY_PATH = os.path.join(INPUT_PATH, 'assembly')
SEQLEN_PATH = os.path.join(INPUT_PATH, 'seqlen')
SUB_PATH = os.path.join(INPUT_PATH, 'submission')
[utils.create_dir_if_not_exists(directory) for directory in [INPUT_PATH, TASK_PATH, INFO_PATH, SEQ_PATH, MODEL_PATH, RAW_PATH, PRED_PATH, LSH_PATH, ASSEMBLY_PATH, SEQLEN_PATH, SUB_PATH ]]

if IS_BRIDGES:
    LOCAL_PATH = os.environ['LOCAL']
    MEMDISK_PATH = os.environ['RAMDISK']
    assert os.path.exists(LOCAL_PATH)
    assert os.path.exists(MEMDISK_PATH)
else:
    LOCAL_PATH = os.path.join(INPUT_PATH, 'local')
    MEMDISK_PATH = os.path.join(INPUT_PATH, 'memdisk')
    [utils.create_dir_if_not_exists(directory) for directory in [LOCAL_PATH, MEMDISK_PATH]]
    
NR_ZIP = os.path.join(INPUT_PATH, 'nr.gz')
NT_ZIP = os.path.join(INPUT_PATH, 'nt.gz')


def get_java_memory(n_thread):
    if IS_BRIDGES:
        mem = n_thread * 30
    else:
        from psutil import virtual_memory
        mem = virtual_memory()
        mem = int(mem.total * 0.8 / 1024 ** 3)    
    return mem


def is_debug():
    return "CAMI2_DEBUG" in os.environ

