import config
from task import Task
from step24a_merge_taxprofile_marmg_submission import merge_samples_taxprof,\
    make_multiple_sample_taxprof

    
def step24b_merge_taxprofile_strmg_submission_illumina(threshold=5):               

    def f(logger):
        merge_samples_taxprof('CAMI2_strmg', 'illumina', threshold,100, logger)
                    
    task = Task('step24b_merge_taxprofile_strmg_submission-th{}'.format(threshold), lambda u: f(u))
    task() 

    
def step24b_merge_taxprofile_strmg_submission_pacbio(threshold=5):               

    def f(logger):
        merge_samples_taxprof('strmgCAMI2', 'pacbio', threshold,100, logger)
                    
    task = Task('step24b_merge_taxprofile_strmg_submission_pacbio-th{}'.format(threshold), lambda u: f(u))
    task()     

def step24b_make_muptile_sample_taxprof_strmg_submission_illumina(threshold=5):               

    def f(logger):
        make_multiple_sample_taxprof('CAMI2_strmg', 'illumina', threshold,100, logger)
                    
    task = Task('step24b_make_muptile_sample_taxprof_strmg_submission_illumina-th{}'.format(threshold), lambda u: f(u))
    task() 

    
def step24b_make_muptile_sample_taxprof_strmg_submission_pacbio(threshold=5):               

    def f(logger):
        make_multiple_sample_taxprof('strmgCAMI2', 'pacbio', threshold,100, logger)
                    
    task = Task('step24b_merge_taxprofile_strmg_submission_pacbio-th{}'.format(threshold), lambda u: f(u))
    task()     

        
def run():
    isDebug = config.is_debug()
    if isDebug:
        pass
    else:
        step24b_merge_taxprofile_strmg_submission_illumina(5)
        step24b_merge_taxprofile_strmg_submission_pacbio(5)
        step24b_make_muptile_sample_taxprof_strmg_submission_illumina(5)
        step24b_make_muptile_sample_taxprof_strmg_submission_pacbio(5)             
        
if __name__ == '__main__':
    run()

