import config
from task import Task
import os
import pandas as pd 
import shutil
import numpy as np  
import gzip
 

def make_multiple_sample_binning(prefix, readtype , n_samples, logger):

    def get_file(sample_no):
            if readtype == 'illumina':
                filename = "{}_sample_{}_{}.species_binner.txt.gz".format(prefix, sample_no, readtype)
                filename = os.path.join(config.SUB_PATH, filename)
                return filename
            elif readtype == 'pacbio':
                filename = "{}_sample_{}_{}.species_binner.txt.gz".format(prefix, sample_no, readtype)
                filename = os.path.join(config.SUB_PATH, filename)
                return filename

    if True:
        samplenos = range(n_samples)

        outputFile = os.path.join(config.SUB_PATH, "{}_multiple_sample_{}.species_binner.txt.gz".format(prefix, readtype))
        if os.path.exists(outputFile):
            print ("output existing, skip: " + outputFile)
            return 
        
        filenames = [get_file(sample_no)                      for sample_no in samplenos]
        
        with gzip.open(outputFile + ".tmp", 'wt') as fout:
            for i, filename in enumerate(filenames):
                with gzip.open(filename, 'rt') as fin:
                    for line in fin:
                        if line.startswith('@Version:'):
                            line = '@Version:0.10.0\n'
                        fout.write(line)
                fout.write("\n")
                
        shutil.move(outputFile + ".tmp", outputFile)    


def step25a_make_multiple_sample_binning_submission_illumina():               

    def f(logger):
        make_multiple_sample_binning('CAMI2_marmg', 'illumina', 10, logger)
                    
    task = Task('step25a_make_multiple_sample_binning_submission_illumina-th{}'.format(), lambda u: f(u))
    task() 

    
def step25a_make_multiple_sample_binning_submission_pacbio():               

    def f(logger):
        make_multiple_sample_binning('marmgCAMI2', 'pacbio', 10, logger)
                    
    task = Task('step25a_make_multiple_sample_binning_submission_pacbio-th{}'.format(), lambda u: f(u))
    task()        

        
def run():
    isDebug = config.is_debug()
    if isDebug:
        pass
    else:
        step25a_make_multiple_sample_binning_submission_illumina()
        step25a_make_multiple_sample_binning_submission_pacbio()        
             
        
if __name__ == '__main__':
    run()

