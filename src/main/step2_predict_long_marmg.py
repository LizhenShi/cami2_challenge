import config
import utils
from task import Task
import os

from utils import check_output_file_exists
from utils import get_input_file


def predict_cami2_pacbio(prefix, logger):               
        
    if True:
        if check_output_file_exists(os.path.join(config.PRED_PATH, '{}.pred'.format(prefix)), logger):
            return
        
        inputModel = os.path.join(config.MODEL_PATH, 'genbank_model_pb_k9', 'model_299')
        assert utils.file_exists(inputModel), inputModel

        hashfile = os.path.join(config.LSH_PATH, 'lsh_CAMI2_pacbio_k9_h25.crp')
        assert utils.file_exists(hashfile), hashfile

        jarpath = os.path.join(config.HOME, "lib", "jfastseq-*-*.jar")
        
        filepath = get_input_file(os.path.join(config.SEQ_PATH, '{}.seq'.format(prefix)), logger)
        
        tmpoutpath = os.path.join(config.LOCAL_PATH, '{}.pred.zst'.format(prefix))
        
        n_thread = utils.get_num_thread()
        cmd = "java -server -Xmx{} -XX:+AggressiveOpts -cp {} net.jfastseq.Main predict --hash {} --input {} --output {} --inputModel {} --max {} --threshold {} --withoutUncult --thread {} ".format(
        utils.get_java_memory(n_thread),
        jarpath,
        hashfile,
        filepath,
        tmpoutpath,
        inputModel,
        500,
        0.005,
        n_thread)
        
        status = utils.shell_run_and_wait2(cmd)
        if status != 0:
            utils.remove_if_file_exit(tmpoutpath)
            msg = "Run command failed: " + cmd
            logger.error(msg)
            raise Exception(msg)
        
        outpath = os.path.join(config.PRED_PATH, '{}.pred.zst'.format(prefix))
        utils.move(tmpoutpath, outpath)


def predict_cami2_marmg_long(fileno):               
        
    def f(logger):
        prefix = 'marmgCAMI2_long_read_sample_{}_reads'.format(fileno)
        predict_cami2_pacbio(prefix, logger)
        
    task = Task('predict_cami2_marmg_long-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_toy_airway_pacbio(fileno):               
        
    def f(logger):
        prefix = 'CAMI_Airways_pacbio_sample_{}'.format(fileno)
        predict_cami2_pacbio(prefix, logger)
                
    task = Task('predict_cami2_toy_airway_pacbio-{}'.format(fileno), lambda u: f(u))
    task() 


def predict_cami2_toy_mousegut_pacbio(fileno):               
        
    def f(logger):
        prefix = 'MOUSEGUT_pacbio_sample_{}'.format(fileno)
        predict_cami2_pacbio(prefix, logger)
                
    task = Task('predict_cami2_toy_mousegut_pacbio-{}'.format(fileno), lambda u: f(u))
    task() 


def make_seq_length_pacbio(prefix, logger):               
     
    if True:
        if check_output_file_exists(os.path.join(config.SEQLEN_PATH, '{}.length'.format(prefix)), logger):
            return
        
        from myzstd import ZstdFile
        import gzip
        import pandas as pd 
        
        inputpath = get_input_file(os.path.join(config.SEQ_PATH, '{}.seq'.format(prefix)), logger)
        tmpoutpath = os.path.join(config.LOCAL_PATH, '{}.length.gz'.format(prefix))
        
        lst = []
        with ZstdFile(inputpath, 'r') if inputpath.endswith('zst') else gzip.open(inputpath, 'rt') as fin:
            for line in fin:
                _, readid, seq = line.split()
                lst.append([readid, len(seq)])
                
        df = pd.DataFrame(lst, columns=['readid', 'seqlen'])
        df[['readid', 'seqlen']].to_csv(tmpoutpath, index=None, compression='gzip')
        
        outpath = os.path.join(config.SEQLEN_PATH, '{}.length.gz'.format(prefix))
        utils.move(tmpoutpath, outpath)


def make_seq_length_marmg_long(fileno):               
        
    def f(logger):
        prefix = 'marmgCAMI2_long_read_sample_{}_reads'.format(fileno)
        make_seq_length_pacbio(prefix, logger)
        
    task = Task('make_seq_length_marmg_long-{}'.format(fileno), lambda u: f(u))
    task() 


def make_seq_length_airway_pacbio(fileno):               
        
    def f(logger):
        prefix = 'CAMI_Airways_pacbio_sample_{}'.format(fileno)
        make_seq_length_pacbio(prefix, logger)
        
    task = Task('make_seq_length_marmg_long-{}'.format(fileno), lambda u: f(u))
    task() 

            
def make_seq_length_mousegut_pacbio(fileno):               
        
    def f(logger):
        prefix = 'MOUSEGUT_pacbio_sample_{}'.format(fileno)
        make_seq_length_pacbio(prefix, logger)
        
    task = Task('make_seq_length_mousegut_pacbio-{}'.format(fileno), lambda u: f(u))
    task()     

        
def run():
    isDebug = config.is_debug()
    
    if not isDebug:
        marmg_nos = [0] if isDebug else range(10)
        for i in marmg_nos:
            # cami2_marmg_long_to_seq(i)
            pass
            
        for i in marmg_nos:
            make_seq_length_marmg_long(i)
            predict_cami2_marmg_long(i)
    else:
        if isDebug:
            toy_airway_nos = [4, 7, 10, 8, 9, 10, 11, 12, 23, 26, 27][:3]
            for i in toy_airway_nos:
                make_seq_length_airway_pacbio(i)
                predict_cami2_toy_airway_pacbio(i)
    
        if isDebug:
            toy_mousegut_nos = [14, 15, 30, 31, 3, 46, 52, 53, 60, 8][:3]
            for i in toy_mousegut_nos:
                make_seq_length_mousegut_pacbio(i)
                predict_cami2_toy_mousegut_pacbio(i)

            
if __name__ == '__main__':
    run()

