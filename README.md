
# CAMI2 Challenge with LSHVec

## Summary
This code is only used to produce submissions  for [CAMI2 Challenge](https://data.cami-challenge.org/participate) with [LSHVec](https://github.com/Lizhen0909/LSHVec).  It is not ready to use for general purpose. 

A few of LSHVec classification models were trained with GenBank bacteria data. Given a read, a  model can predict the probabilities that the read belongs to the taxonomic nodes of NCBI taxonomic tree.   Due to time and resource restriction,  only one strain was sampled from each species, which means it does not work on strain level.

The model files is too big to be put into a  code repository. I am finding a place to put it. Currently it locates in Bridge HPC.  

I plans to make  ready-to-use pre-trained models and python bindings in the future. Please check here later. 

## Requirements


1. Docker 
2. tar file of pre-trained models (lsh_cami2_input.tar) (not included here)

## Resources

1. The code was ever run on a Bridges large memory node and an aws R5 instance.  It demands more memory than cores.
2. When running scripts of illumina reads, there is a step which loads all prediction into memory and processes it with python multiprocess API. This is not efficient and may fail with memory error. Try 8 cores or 4 cores with 512G memory
3. By default it uses max_num_core -1 for parallell processing. To control the number of cores set OMP_NUM_THREADS environment variable for docker. 
4. 1 GB disk space is required (more space demands if running jobs of illumina reads)  
5. Running all the scripts takes quite a few days. However a script has several steps, which makes it possible to stop running and resume later from the stopped step. 
6. Some steps are shared, so it is better to run them on same node.     
 

## How to run

1. Prepares a disk that is big enough since CAMI2 dataset is huge. 
2. Make a data folder (e.g. /mnt/data/cami2) on host. Let's refer it as $HOST_DATA below.
3. Put pre-trained models (which is included in lsh_cami2_input.tar)  in $HOST_DATA. A few folders will appear in  $HOST_DATA (e.g. model folder)
4. Put CAMI2 reads files( a.k.a *.fq.gz) in $HOST_DATA/raw folder
4. Put CAMI2 assembly files( a.k.a *assembly.fasta.gz) in $HOST_DATA/assembly folder
5. Pull latest docker 

		docker pull lizhen0909/cami2:latest
	
6. Run docker command. For example

		docker run --rm -v $HOST_DATA:/cami2_challenge/input \
		lizhen0909/cami2:latest $A_PYTHON_SCRIPT # see script list in the section below 

## Content of tar file of pre-train models

Uncompressing  lsh_cami2_input.tar file will make a few folders in $HOST_DATA

1. $HOST_DATA/model:  model files (where k-mer embedding locates)
2. $HOST_DATA/lsh:       locality-sensitive-hashing file
3. $HOST_DATA/info:    a few prepossessed csv files from NCBI taxonomy tree, which help to make submission.
4. $HOST_DATA/assembly:     patmg assemblies with spades (only useful for patmg challenge)

## Scripts

### For Marmg

| Script                                                        | Type    | Use Data | Description               |
|---------------------------------------------------------------|---------|----------|---------------------------|
| run_binning_cami2_assembly_marmg_gold_standard.py             | binning | GS       |                           |
| run_binning_cami2_assembly_marmg_megahit.py                   | binning | Meghit   |                           |
| run_binning_cami2_marmg_illumina_reads.py                     | binning | illumina |                           |
| run_binning_cami2_marmg_pacbio_reads.py                       | binning | pacbio   |                           |
| run_taxonomic_profiling_cami2_assembly_marmg_gold_standard.py | TaxProf | GS       |                           |
| run_taxonomic_profiling_cami2_assembly_marmg_megahit.py       | TaxProf | Meghit   |                           |
| run_taxonomic_profiling_cami2_marmg_illumina_reads_pool.py    | TaxProf | illumina | tax pf of pool of samples |
| run_taxonomic_profiling_cami2_marmg_illumina_reads.py         | TaxProf | illumina | tax pf of each sample     |
| run_taxonomic_profiling_cami2_marmg_pacbio_reads_pool.py      | TaxProf | pacbio   | tax pf of pool of samples |
| run_taxonomic_profiling_cami2_marmg_pacbio_reads.py           | TaxProf | pacbio   | tax pf of each sample     |

### For  Strmg
 
| Script                                                             | Type    | Use Data | Description               |
|--------------------------------------------------------------------|---------|----------|---------------------------|
| run_binning_cami2_assembly_strmg_gold_standard_fine.py             | binning | GS       | strain level              |
| run_binning_cami2_assembly_strmg_gold_standard.py                  | binning | GS       |                           |
| run_binning_cami2_assembly_strmg_megahit.py                        | binning | Meghit   |                           |
| run_binning_cami2_strmg_illumina_reads.py                          | binning | illumina | *big output               |
| run_binning_cami2_strmg_pacbio_reads.py                            | binning | pacbio   |                           |
| run_taxonomic_profiling_cami2_assembly_strmg_gold_standard_fine.py | TaxProf | GS       | strain level              |
| run_taxonomic_profiling_cami2_assembly_strmg_gold_standard.py      | TaxProf | GS       |                           |
| run_taxonomic_profiling_cami2_assembly_strmg_megahit.py            | TaxProf | Meghit   |                           |
| run_taxonomic_profiling_cami2_strmg_illumina_reads_pool.py         | TaxProf | illumina | tax pf of pool of samples |
| run_taxonomic_profiling_cami2_strmg_illumina_reads.py              | TaxProf | illumina | tax pf of each sample     |
| run_taxonomic_profiling_cami2_strmg_pacbio_reads_pool.py           | TaxProf | pacbio   | tax pf of pool of samples |
| run_taxonomic_profiling_cami2_strmg_pacbio_reads.py                | TaxProf | pacbio   | tax pf of each sample     |


### For Patmg

| Script                                 | Type | Use Data | Description |
|----------------------------------------|------|----------|-------------|
| run_taxonomic_profiling_cami2_patmg.py |      |          | for patmg   |


